# **Compétition** #

# Description

Cet outil permet de gérer des courses, des partcipants et des groupes afin d'établir un classsement chronométré imprimable.


# Screenshot

![Compétition.png](http://i.imgur.com/v2YGQF4.png)


# Features

* Affichage des courses existantes
* Création/suppression/modification de courses
* Création/suppression/modification de particpants
* Création/suppression/modification de groupes personnalisables
* Départ/arrêt du chronométre
* Pointage des arrivants (F1) avec enregistrement de dossard
* Arrêt du chronomètre selon le nombre de particpant pointé par numéro de dossard
* Affichage du classement définitif
* Affichage des personnes non pointés (en fin de course) selon la liste des particpants
* Paramétrage de l'affichage de l'impression : redimensionnement puis affichage/masquage et/ou filtrage croissant/décroissant des colonnes.


# Prerequisites

* Windows Vista, 7, 8, 10
* DotNet Framework 3.5
* Logiciel portable, ne requiert pas d'installation !


# Copyright

Copyright © HrtJm 2008-2018


# Licence

Free to use and modify