﻿Imports System.Threading
Imports System.Management

Friend Class Program

#Region "Sub Main"
    <STAThread()>
    Public Shared Sub Main(ByVal Args As String())
        Dim instanceCountOne As Boolean = False
        Using mtex As Mutex = New Mutex(True, Application.ProductName, instanceCountOne)
            If instanceCountOne Then
                Application.EnableVisualStyles()
                Application.SetCompatibleTextRenderingDefault(False)
                Application.Run(New FrmMain)
                mtex.ReleaseMutex()
            End If
        End Using
    End Sub
#End Region

End Class
