﻿
Namespace Extensions
    ''' <summary>
    ''' Event Args for SubItemEndEditingClicked event
    ''' </summary>
    Public Class SubItemEndEditingEventArgs
        Inherits SubItemEventArgs
        Private _text As String = String.Empty
        Private _cancel As Boolean = True

        Public Sub New(item As ListViewItem, subItem As Integer, display As String, cancel As Boolean)
            MyBase.New(item, subItem)
            _text = display
            _cancel = cancel
        End Sub
        Public Property DisplayText() As String
            Get
                Return _text
            End Get
            Set(value As String)
                _text = value
            End Set
        End Property
        Public Property Cancel() As Boolean
            Get
                Return _cancel
            End Get
            Set(value As Boolean)
                _cancel = value
            End Set
        End Property
    End Class
End Namespace
