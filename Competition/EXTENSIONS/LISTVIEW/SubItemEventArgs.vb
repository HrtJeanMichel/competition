﻿
Namespace Extensions
    ''' <summary>
    ''' Event Args for SubItemClicked event
    ''' </summary>
    Public Class SubItemEventArgs
        Inherits EventArgs
        Public Sub New(item As ListViewItem, subItem As Integer)
            _subItemIndex = subItem
            _item = item
        End Sub
        Private _subItemIndex As Integer = -1
        Private _item As ListViewItem = Nothing
        Public ReadOnly Property SubItem() As Integer
            Get
                Return _subItemIndex
            End Get
        End Property
        Public ReadOnly Property Item() As ListViewItem
            Get
                Return _item
            End Get
        End Property
    End Class
End Namespace
