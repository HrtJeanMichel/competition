﻿Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Data
Imports System.Windows.Forms
Imports System.Runtime.InteropServices
Imports System.Security.Permissions
Imports System.Drawing.Printing

Namespace Extensions
    ''' <summary>
    ''' Event Handler for SubItem events
    ''' </summary>
    Public Delegate Sub SubItemEventHandler(sender As Object, e As SubItemEventArgs)
    ''' <summary>
    ''' Event Handler for SubItemEndEditing events
    ''' </summary>
    Public Delegate Sub SubItemEndEditingEventHandler(sender As Object, e As SubItemEndEditingEventArgs)

    '''	<summary>
    '''	Inherited ListView to allow in-place editing of subitems
    '''	</summary>
    Public Class ListViewEx
        Inherits ListView

#Region " PRINT FIELDS "
        ' Print fields
        Private m_printDoc As PrintDocument
        Private m_setupDlg As PageSetupDialog
        Private m_previewDlg As PrintPreviewDialog
        Private m_printDlg As PrintDialog

        Private m_nPageNumber As Integer = 1
        Private m_nStartRow As Integer = 0
        Private m_nStartCol As Integer = 0

        Private m_bPrintSel As Boolean = False

        Private m_bFitToPage As Boolean = False

        Private m_fListWidth As Single = 0.0F
        Private m_arColsWidth As Single()

        Private m_fDpi As Single = 96.0F

        Private m_strTitle As String = ""
#End Region

#Region " PRINT PROPERTIES "
        ''' <summary>
        '''		Gets or sets whether to fit the list width on a single page
        ''' </summary>
        ''' <value>
        '''		<c>True</c> if you want to scale the list width so it will fit on a single page.
        ''' </value>
        ''' <remarks>
        '''		If you choose false (the default value), and the list width exceeds the page width, the list
        '''		will be broken in multiple page.
        ''' </remarks>
        Public Property FitToPage() As Boolean
            Get
                Return m_bFitToPage
            End Get
            Set(value As Boolean)
                m_bFitToPage = value
            End Set
        End Property

        ''' <summary>
        '''		Gets or sets the title to dispaly as page header in the printed list
        ''' </summary>
        ''' <value>
        '''		A <see cref="string"/> the represents the title printed as page header.
        ''' </value>
        Public Property Title() As String
            Get
                Return m_strTitle
            End Get
            Set(value As String)
                m_strTitle = value
            End Set
        End Property
#End Region

#Region "Interop structs, imports and constants"
        ''' <summary>
        ''' MessageHeader for WM_NOTIFY
        ''' </summary>
        Private Structure NMHDR
            Public hwndFrom As IntPtr
            Public idFrom As Int32
            Public code As Int32
        End Structure

        <DllImport("user32.dll")> _
        Private Shared Function SendMessage(hWnd As IntPtr, msg As Integer, wPar As IntPtr, lPar As IntPtr) As IntPtr
        End Function

        <DllImport("user32.dll", CharSet:=CharSet.Ansi)> _
        Private Shared Function SendMessage(hWnd As IntPtr, msg As Integer, len As Integer, ByRef order As Integer()) As IntPtr
        End Function

        <DllImport("uxtheme.dll", CharSet:=CharSet.Unicode)> _
        Private Shared Function SetWindowTheme(hWnd As IntPtr, pszSubAppName As String, pszSubIdList As String) As Integer
        End Function

        ' ListView messages
        Private Const LVM_FIRST As Integer = &H1000
        Private Const LVM_GETCOLUMNORDERARRAY As Integer = (LVM_FIRST + 59)
        Private Const LVM_SETEXTENDEDLISTVIEWSTYLE As Integer = LVM_FIRST + 54
        Private Const LVS_EX_DOUBLEBUFFER As Integer = 65536
        Private Const WM_PAINT As Integer = 15

        ' Windows Messages that will abort editing
        Private Const WM_HSCROLL As Integer = &H114
        Private Const WM_VSCROLL As Integer = &H115
        Private Const WM_SIZE As Integer = &H5
        Private Const WM_NOTIFY As Integer = &H4E

        Private Const HDN_FIRST As Integer = -300
        Private Const HDN_BEGINDRAG As Integer = (HDN_FIRST - 10)
        Private Const HDN_ITEMCHANGINGA As Integer = (HDN_FIRST - 0)
        Private Const HDN_ITEMCHANGINGW As Integer = (HDN_FIRST - 20)

        Private Const LVM_DELETEITEM As Integer = LVM_FIRST + 8
        Private Const LVM_DELETEALLITEMS As Integer = LVM_FIRST + 9
        Private Const LVM_INSERTITEMA As Integer = LVM_FIRST + 7
        Private Const LVM_INSERTITEMW As Integer = LVM_FIRST + 77

#End Region

        '''	<summary>
        '''	Required designer variable.
        '''	</summary>
        Private components As System.ComponentModel.Container = Nothing
        Private m_elv As Boolean = False

        Public Event SubItemClicked As SubItemEventHandler
        Public Event SubItemBeginEditing As SubItemEventHandler
        Public Event SubItemEndEditing As SubItemEndEditingEventHandler

        Public Sub New()
            ' This	call is	required by	the	Windows.Forms Form Designer.
            InitializeComponent()

            MyBase.FullRowSelect = True
            MyBase.View = View.Details
            MyBase.AllowColumnReorder = True
            Me.ShowGroups = False

            ' PRINT instanciations
            m_printDoc = New PrintDocument()
            m_setupDlg = New PageSetupDialog()
            m_previewDlg = New PrintPreviewDialog()
            m_printDlg = New PrintDialog()

            ' PRINT Declarations
            AddHandler m_printDoc.BeginPrint, New PrintEventHandler(AddressOf OnBeginPrint)
            AddHandler m_printDoc.PrintPage, New PrintPageEventHandler(AddressOf OnPrintPage)

            m_setupDlg.Document = m_printDoc

            m_previewDlg.Document = m_printDoc
            m_previewDlg.ShowIcon = False
            m_previewDlg.WindowState = FormWindowState.Maximized

            m_printDlg.Document = m_printDoc
            m_printDlg.AllowSomePages = False
            m_printDlg.UseEXDialog = True
        End Sub

#Region " PRINT METHODS "
        ''' <summary>
        '''		Show the standard page setup dialog box that lets the user specify
        '''		margins, page orientation, page sources, and paper sizes.
        ''' </summary>
        Public Sub PageSetup()
            m_setupDlg.ShowDialog()
        End Sub

        ''' <summary>
        '''		Show the standard print preview dialog box.
        ''' </summary>
        Public Sub PrintPreview()
            m_printDoc.DocumentName = m_strTitle

            m_nPageNumber = 1
            m_bPrintSel = False

            m_previewDlg.ShowDialog(Me)
        End Sub

        ''' <summary>
        '''		Start the print process.
        ''' </summary>
        Public Sub Print()
            Try
                If m_printDlg.ShowDialog(Me) = DialogResult.OK Then
                    m_printDoc.DocumentName = m_strTitle
                    m_bPrintSel = m_printDlg.PrinterSettings.PrintRange = PrintRange.Selection
                    m_nPageNumber = 1
                    m_printDoc.Print()
                End If
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End Sub

        Private Function GetItemsCount() As Integer
            Return If(m_bPrintSel, SelectedItems.Count, Items.Count)
        End Function

        Private Function GetItem(index As Integer) As ListViewItem
            Return If(m_bPrintSel, SelectedItems(index), Items(index))
        End Function

        Private Sub PreparePrint()
            ' Gets the list width and the columns width in units of hundredths of an inch.
            Me.m_fListWidth = 0.0F
            Me.m_arColsWidth = New Single(Me.Columns.Count - 1) {}

            Dim g As Graphics = CreateGraphics()
            m_fDpi = g.DpiX
            g.Dispose()

            For i As Integer = 0 To Columns.Count - 1
                Dim ch As ColumnHeader = Columns(i)
                Dim fWidth As Single = ch.Width / m_fDpi * 100 + 1
                ' Column width + separator
                m_fListWidth += fWidth
                m_arColsWidth(i) = fWidth
            Next
            m_fListWidth += 1
            ' separator
        End Sub
#End Region

#Region " PRINT EVENT HANDLERS "
        Private Sub OnBeginPrint(sender As Object, e As PrintEventArgs)
            PreparePrint()
        End Sub

        Private Sub OnPrintPage(sender As Object, e As PrintPageEventArgs)
            Dim nNumItems As Integer = GetItemsCount()
            ' Number of items to print
            If nNumItems = 0 OrElse m_nStartRow >= nNumItems Then
                e.HasMorePages = False
                Return
            End If

            Dim nNextStartCol As Integer = 0
            ' First column exeeding the page width
            Dim x As Single = 0.0F
            ' Current horizontal coordinate
            Dim y As Single = 0.0F
            ' Current vertical coordinate
            Dim cx As Single = 4.0F
            ' The horizontal space, in hundredths of an inch,
            ' of the padding between items text and
            ' their cell boundaries.
            Dim fScale As Single = 1.0F
            ' Scale factor when fit to page is enabled
            Dim fRowHeight As Single = 0.0F
            ' The height of the current row
            Dim fColWidth As Single = 0.0F
            ' The width of the current column
            Dim rectFull As RectangleF
            ' The full available space
            Dim rectBody As RectangleF
            ' Area for the list items
            Dim bUnprintable As Boolean = False

            Dim g As Graphics = e.Graphics

            If g.VisibleClipBounds.X < 0 Then
                ' Print preview
                rectFull = e.MarginBounds

                ' Convert to hundredths of an inch
                rectFull = New RectangleF(rectFull.X / m_fDpi * 100.0F, rectFull.Y / m_fDpi * 100.0F, rectFull.Width / m_fDpi * 100.0F, rectFull.Height / m_fDpi * 100.0F)
            Else
                ' Print
                ' Printable area (approximately) of the page, taking into account the user margins
                rectFull = New RectangleF(e.MarginBounds.Left - (e.PageBounds.Width - g.VisibleClipBounds.Width) / 2, e.MarginBounds.Top - (e.PageBounds.Height - g.VisibleClipBounds.Height) / 2, e.MarginBounds.Width, e.MarginBounds.Height)
            End If

            rectBody = RectangleF.Inflate(rectFull, 0, -2 * Font.GetHeight(g))

            ' Display title at top
            Dim sfmt As New StringFormat()
            sfmt.Alignment = StringAlignment.Center
            g.DrawString(m_strTitle, Font, Brushes.Black, rectFull, sfmt)

            ' Display page number at bottom
            sfmt.LineAlignment = StringAlignment.Far
            g.DrawString("Page " & m_nPageNumber, Font, Brushes.Black, rectFull, sfmt)

            If m_nStartCol = 0 AndAlso m_bFitToPage AndAlso m_fListWidth > rectBody.Width Then
                ' Calculate scale factor
                fScale = rectBody.Width / m_fListWidth
            End If

            ' Scale the printable area
            rectFull = New RectangleF(rectFull.X / fScale, rectFull.Y / fScale, rectFull.Width / fScale, rectFull.Height / fScale)

            rectBody = New RectangleF(rectBody.X / fScale, rectBody.Y / fScale, rectBody.Width / fScale, rectBody.Height / fScale)

            ' Setting scale factor and unit of measure
            g.ScaleTransform(fScale, fScale)
            g.PageUnit = GraphicsUnit.Inch
            g.PageScale = 0.01F

            ' Start print
            nNextStartCol = 0
            y = rectBody.Top

            ' Columns headers ----------------------------------------
            Dim brushHeader As Brush = New SolidBrush(Color.LightGray)
            Dim fontHeader As New Font(Me.Font, FontStyle.Bold)
            fRowHeight = fontHeader.GetHeight(g) * 3.0F
            x = rectBody.Left

            For i As Integer = m_nStartCol To Columns.Count - 1
                Dim ch As ColumnHeader = Columns(i)
                fColWidth = m_arColsWidth(i)

                If (x + fColWidth) <= rectBody.Right Then
                    ' Rectangle
                    g.FillRectangle(brushHeader, x, y, fColWidth, fRowHeight)
                    g.DrawRectangle(Pens.Black, x, y, fColWidth, fRowHeight)

                    ' Text
                    Dim sf As New StringFormat()
                    If ch.TextAlign = HorizontalAlignment.Left Then
                        sf.Alignment = StringAlignment.Near
                    ElseIf ch.TextAlign = HorizontalAlignment.Center Then
                        sf.Alignment = StringAlignment.Center
                    Else
                        sf.Alignment = StringAlignment.Far
                    End If

                    sf.LineAlignment = StringAlignment.Center
                    sf.FormatFlags = StringFormatFlags.NoWrap
                    sf.Trimming = StringTrimming.EllipsisCharacter

                    Dim rectText As New RectangleF(x + cx, y, fColWidth - 1 - 2 * cx, fRowHeight)
                    g.DrawString(ch.Text, fontHeader, Brushes.Black, rectText, sf)
                    x += fColWidth
                Else
                    If i = m_nStartCol Then
                        bUnprintable = True
                    End If

                    nNextStartCol = i
                    Exit For
                End If
            Next
            y += fRowHeight

            ' Rows ---------------------------------------------------
            Dim nRow As Integer = m_nStartRow
            Dim bEndOfPage As Boolean = False
            While Not bEndOfPage AndAlso nRow < nNumItems
                Dim item As ListViewItem = GetItem(nRow)

                fRowHeight = item.Bounds.Height / m_fDpi * 100.0F + 5.0F

                If y + fRowHeight > rectBody.Bottom Then
                    bEndOfPage = True
                Else
                    x = rectBody.Left

                    For i As Integer = m_nStartCol To Columns.Count - 1
                        Dim ch As ColumnHeader = Columns(i)
                        fColWidth = m_arColsWidth(i)

                        If (x + fColWidth) <= rectBody.Right Then
                            ' Rectangle
                            g.DrawRectangle(Pens.Black, x, y, fColWidth, fRowHeight)

                            ' Text
                            Dim sf As New StringFormat()
                            If ch.TextAlign = HorizontalAlignment.Left Then
                                sf.Alignment = StringAlignment.Near
                            ElseIf ch.TextAlign = HorizontalAlignment.Center Then
                                sf.Alignment = StringAlignment.Center
                            Else
                                sf.Alignment = StringAlignment.Far
                            End If

                            sf.LineAlignment = StringAlignment.Center
                            sf.FormatFlags = StringFormatFlags.NoWrap
                            sf.Trimming = StringTrimming.EllipsisCharacter

                            ' Text
                            Dim strText As String = If(i = 0, item.Text, item.SubItems(i).Text)
                            Dim font__1 As Font = If(i = 0, item.Font, item.SubItems(i).Font)

                            Dim rectText As New RectangleF(x + cx, y, fColWidth - 1 - 2 * cx, fRowHeight)
                            g.DrawString(strText, font__1, Brushes.Black, rectText, sf)
                            x += fColWidth
                        Else
                            nNextStartCol = i
                            Exit For
                        End If
                    Next

                    y += fRowHeight
                    nRow += 1
                End If
            End While

            If nNextStartCol = 0 Then
                m_nStartRow = nRow
            End If

            m_nStartCol = nNextStartCol

            m_nPageNumber += 1

            e.HasMorePages = (Not bUnprintable AndAlso (m_nStartRow > 0 AndAlso m_nStartRow < nNumItems) OrElse m_nStartCol > 0)

            If Not e.HasMorePages Then
                m_nPageNumber = 1
                m_nStartRow = 0
                m_nStartCol = 0
            End If

            brushHeader.Dispose()
        End Sub
#End Region

        '''	<summary>
        '''	Clean up any resources being used.
        '''	</summary>
        Protected Overrides Sub Dispose(disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

#Region "Component	Designer generated code"
        '''	<summary>
        '''	Required method	for	Designer support - do not modify 
        '''	the	contents of	this method	with the code editor.
        '''	</summary>
        Private Sub InitializeComponent()
            components = New System.ComponentModel.Container()
        End Sub
#End Region

        Private _doubleClickActivation As Boolean = False
        ''' <summary>
        ''' Is a double click required to start editing a cell?
        ''' </summary>
        Public Property DoubleClickActivation() As Boolean
            Get
                Return _doubleClickActivation
            End Get
            Set(value As Boolean)
                _doubleClickActivation = value
            End Set
        End Property


        ''' <summary>
        ''' Retrieve the order in which columns appear
        ''' </summary>
        ''' <returns>Current display order of column indices</returns>
        Public Function GetColumnOrder() As Integer()
            Dim lPar As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(GetType(Integer)) * Columns.Count)

            Dim res As IntPtr = SendMessage(Handle, LVM_GETCOLUMNORDERARRAY, New IntPtr(Columns.Count), lPar)
            If res.ToInt32() = 0 Then
                ' Something went wrong
                Marshal.FreeHGlobal(lPar)
                Return Nothing
            End If

            Dim order As Integer() = New Integer(Columns.Count - 1) {}
            Marshal.Copy(lPar, order, 0, Columns.Count)

            Marshal.FreeHGlobal(lPar)

            Return order
        End Function


        ''' <summary>
        ''' Find ListViewItem and SubItem Index at position (x,y)
        ''' </summary>
        ''' <param name="x">relative to ListView</param>
        ''' <param name="y">relative to ListView</param>
        ''' <param name="item">Item at position (x,y)</param>
        ''' <returns>SubItem index</returns>
        Public Function GetSubItemAt(x As Integer, y As Integer, ByRef item As ListViewItem) As Integer
            item = Me.GetItemAt(x, y)

            If item IsNot Nothing Then
                Dim order As Integer() = GetColumnOrder()
                Dim lviBounds As Rectangle
                Dim subItemX As Integer

                lviBounds = item.GetBounds(ItemBoundsPortion.Entire)
                subItemX = lviBounds.Left
                For i As Integer = 0 To order.Length - 1
                    Dim h As ColumnHeader = Me.Columns(order(i))
                    If x < subItemX + h.Width Then
                        Return h.Index
                    End If
                    subItemX += h.Width
                Next
            End If

            Return -1
        End Function


        ''' <summary>
        ''' Get bounds for a SubItem
        ''' </summary>
        ''' <param name="Item">Target ListViewItem</param>
        ''' <param name="SubItem">Target SubItem index</param>
        ''' <returns>Bounds of SubItem (relative to ListView)</returns>
        Public Function GetSubItemBounds(Item As ListViewItem, SubItem As Integer) As Rectangle
            Dim order As Integer() = GetColumnOrder()

            Dim subItemRect As Rectangle = Rectangle.Empty
            If SubItem >= order.Length Then
                Throw New IndexOutOfRangeException("SubItem " + SubItem + " out of range")
            End If

            If Item Is Nothing Then
                Throw New ArgumentNullException("Item")
            End If

            Dim lviBounds As Rectangle = Item.GetBounds(ItemBoundsPortion.Entire)
            Dim subItemX As Integer = lviBounds.Left

            Dim col As ColumnHeader
            Dim i As Integer
            For i = 0 To order.Length - 1
                col = Me.Columns(order(i))
                If col.Index = SubItem Then
                    Exit For
                End If
                subItemX += col.Width
            Next
            subItemRect = New Rectangle(subItemX, lviBounds.Top, Me.Columns(order(i)).Width, lviBounds.Height)
            Return subItemRect
        End Function

        Private Sub SetWindowTheme()
            SetWindowTheme(Me.Handle, "explorer", Nothing)
            SendMessage(Me.Handle, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_DOUBLEBUFFER, LVS_EX_DOUBLEBUFFER)
        End Sub

        <SecurityPermission(SecurityAction.LinkDemand, Flags:=SecurityPermissionFlag.UnmanagedCode)> _
        Protected Overrides Sub WndProc(ByRef msg As Message)
            ' Look	for	WM_VSCROLL,WM_HSCROLL or WM_SIZE messages.
            Select Case msg.Msg
                Case WM_PAINT
                    If Not m_elv Then
                        SetWindowTheme()
                        m_elv = True
                    End If
                    Exit Select
                Case WM_VSCROLL, WM_HSCROLL, WM_SIZE
                    EndEditing(False)
                    Exit Select
                Case WM_NOTIFY
                    ' Look for WM_NOTIFY of events that might also change the
                    ' editor's position/size: Column reordering or resizing
                    Dim h As NMHDR = CType(Marshal.PtrToStructure(msg.LParam, GetType(NMHDR)), NMHDR)
                    If h.code = HDN_BEGINDRAG OrElse h.code = HDN_ITEMCHANGINGA OrElse h.code = HDN_ITEMCHANGINGW Then
                        EndEditing(False)
                    End If
                    Exit Select
            End Select

            MyBase.WndProc(msg)
        End Sub


#Region "Initialize editing depending of DoubleClickActivation property"
        Protected Overrides Sub OnMouseUp(e As System.Windows.Forms.MouseEventArgs)
            MyBase.OnMouseUp(e)

            If DoubleClickActivation Then
                Return
            End If

            EditSubitemAt(New Point(e.X, e.Y))
        End Sub

        Protected Overrides Sub OnDoubleClick(e As EventArgs)
            MyBase.OnDoubleClick(e)

            If Not DoubleClickActivation Then
                Return
            End If

            Dim pt As Point = Me.PointToClient(Cursor.Position)

            EditSubitemAt(pt)
        End Sub

        '''<summary>
        ''' Fire SubItemClicked
        '''</summary>
        '''<param name="p">Point of click/doubleclick</param>
        Private Sub EditSubitemAt(p As Point)
            Dim item As ListViewItem = Nothing
            Dim idx As Integer = GetSubItemAt(p.X, p.Y, item)
            If idx >= 0 Then
                OnSubItemClicked(New SubItemEventArgs(item, idx))
            End If
        End Sub

#End Region

#Region "In-place editing functions"
        ' The control performing the actual editing
        Private _editingControl As Control
        ' The LVI being edited
        Private _editItem As ListViewItem
        ' The SubItem being edited
        Private _editSubItem As Integer

        Protected Sub OnSubItemBeginEditing(e As SubItemEventArgs)
            RaiseEvent SubItemBeginEditing(Me, e)
        End Sub
        Protected Sub OnSubItemEndEditing(e As SubItemEndEditingEventArgs)
            RaiseEvent SubItemEndEditing(Me, e)
        End Sub
        Protected Sub OnSubItemClicked(e As SubItemEventArgs)
            RaiseEvent SubItemClicked(Me, e)
        End Sub


        ''' <summary>
        ''' Begin in-place editing of given cell
        ''' </summary>
        ''' <param name="c">Control used as cell editor</param>
        ''' <param name="Item">ListViewItem to edit</param>
        ''' <param name="SubItem">SubItem index to edit</param>
        Public Sub StartEditing(c As Control, Item As ListViewItem, SubItem As Integer)
            OnSubItemBeginEditing(New SubItemEventArgs(Item, SubItem))

            Dim rcSubItem As Rectangle = GetSubItemBounds(Item, SubItem)

            If rcSubItem.X < 0 Then
                ' Left edge of SubItem not visible - adjust rectangle position and width
                rcSubItem.Width += rcSubItem.X
                rcSubItem.X = 0
            End If
            If rcSubItem.X + rcSubItem.Width > Me.Width Then
                ' Right edge of SubItem not visible - adjust rectangle width
                rcSubItem.Width = Me.Width - rcSubItem.Left
            End If

            ' Subitem bounds are relative to the location of the ListView!
            rcSubItem.Offset(Left, Top)

            ' In case the editing control and the listview are on different parents,
            ' account for different origins
            Dim origin As New Point(0, 0)
            Dim lvOrigin As Point = Me.Parent.PointToScreen(origin)
            Dim ctlOrigin As Point = c.Parent.PointToScreen(origin)

            rcSubItem.Offset(lvOrigin.X - ctlOrigin.X, lvOrigin.Y - ctlOrigin.Y)

            ' Position and show editor
            c.Bounds = rcSubItem
            c.Text = Item.SubItems(SubItem).Text
            c.Visible = True
            c.BringToFront()
            c.Focus()

            _editingControl = c
            AddHandler _editingControl.Leave, New EventHandler(AddressOf _editControl_Leave)
            AddHandler _editingControl.KeyPress, New KeyPressEventHandler(AddressOf _editControl_KeyPress)

            _editItem = Item
            _editSubItem = SubItem
        End Sub


        Private Sub _editControl_Leave(sender As Object, e As EventArgs)
            ' cell editor losing focus
            EndEditing(True)
        End Sub

        Private Sub _editControl_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs)
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Keys.Escape)))
                    If True Then
                        EndEditing(False)
                        Exit Select
                    End If

                Case CChar(ChrW(CInt(Keys.Enter)))
                    If True Then
                        EndEditing(True)
                        Exit Select
                    End If
            End Select
        End Sub

        ''' <summary>
        ''' Accept or discard current value of cell editor control
        ''' </summary>
        ''' <param name="AcceptChanges">Use the _editingControl's Text as new SubItem text or discard changes?</param>
        Public Sub EndEditing(AcceptChanges As Boolean)
            If _editingControl Is Nothing Then
                Return
            End If

            ' The item being edited
            ' The subitem index being edited
            ' Use editControl text if changes are accepted
            ' or the original subitem's text, if changes are discarded
            ' Cancel?
            Dim e As New SubItemEndEditingEventArgs(_editItem, _editSubItem, If(AcceptChanges, _editingControl.Text, _editItem.SubItems(_editSubItem).Text), Not AcceptChanges)

            OnSubItemEndEditing(e)

            _editItem.SubItems(_editSubItem).Text = e.DisplayText

            RemoveHandler _editingControl.Leave, New EventHandler(AddressOf _editControl_Leave)
            RemoveHandler _editingControl.KeyPress, New KeyPressEventHandler(AddressOf _editControl_KeyPress)

            _editingControl.Visible = False

            _editingControl = Nothing
            _editItem = Nothing
            _editSubItem = -1
        End Sub
#End Region

        '#Region " ############################################# GroupByOrder "

        '        ##################################### ok

        '#Region " Variables "
        '        Public isRunningXPOrLater As Boolean = OSFeature.Feature.IsPresent(OSFeature.Themes)
        '        ' Declare a Hashtable array in which to store the groups.
        '        Public groupTables() As Hashtable
        '        ' Declare a variable to store the current grouping column.
        '        Private groupColumn As Integer = 0
        '#End Region

        '#Region " Methods "
        '        Overridable Sub Cpt_ListViewEx_ColumnClick(ByVal sender As System.Object, ByVal e As ColumnClickEventArgs) Handles MyBase.ColumnClick
        '            ' Set the sort order to ascending when changing
        '            ' column groups; otherwise, reverse the sort order.


        '            If MyBase.Items.Count <> 0 Then
        '                'MyBase.Sorting = If((MyBase.Sorting = SortOrder.Descending) OrElse _
        '                '               (isRunningXPOrLater And e.Column <> groupColumn) _
        '                '               = True, SortOrder.Ascending, SortOrder.Descending)
        '                'groupColumn = e.Column
        '                'MsgBox(e.Column.ToString)
        '                'SetGroups(e.Column, MyBase.Sorting)
        '                GroupListView(e.Column)
        '            End If
        '        End Sub

        '        Private Sub GroupListView(SubItemIndex As Integer)
        '            Dim flag As Boolean = True
        '            For Each l As ListViewItem In Me.Items
        '                Dim strmyGroupname As String = l.SubItems(SubItemIndex).Text
        '                For Each lvg As ListViewGroup In Me.Groups
        '                    If lvg.Name = strmyGroupname Then
        '                        l.Group = lvg
        '                        flag = False
        '                    End If
        '                Next
        '                If flag = True Then
        '                    Dim lstGrp As New ListViewGroup(strmyGroupname, strmyGroupname)
        '                    Me.Groups.Add(lstGrp)
        '                    l.Group = lstGrp
        '                End If
        '                flag = True
        '            Next
        '        End Sub

        '        ' Sets myListView to the groups created for the specified column.
        '        Public Sub SetGroups(column%, sorting%)
        '            Dim new_sorting_column As ColumnHeader = MyBase.Columns(column)
        '            Dim m_SortingColumn As ColumnHeader
        '            m_SortingColumn = new_sorting_column

        '            '################## ADDED : PREFIX COLUMN HEADER ###########
        '            Dim prefix$ = If(sorting = 2, "<-> ", "<+> ")
        '            For Each c As ColumnHeader In MyBase.Columns
        '                If c.Text.StartsWith("<-> ") Then c.Text = c.Text.Replace("<-> ", "")
        '                If c.Text.StartsWith("<+> ") Then c.Text = c.Text.Replace("<+> ", "")
        '            Next
        '            m_SortingColumn.Text = If(new_sorting_column.Text.StartsWith(prefix), new_sorting_column.Text.Replace(prefix, ""), prefix & m_SortingColumn.Text)
        '            '###########################################################

        '            ' Remove the current groups.
        '            Me.Groups.Clear()

        '            ' Retrieve the hash table corresponding to the column.
        '            Dim groups As Hashtable = CType(groupTables(column), Hashtable)

        '            If groups Is Nothing Then
        '                Exit Sub
        '            End If
        '            ' Copy the groups for the column to an array.
        '            Dim groupsArray(groups.Count - 1) As ListViewGroup
        '            groups.Values.CopyTo(groupsArray, 0)

        '            ' Sort the groups and add them to myListView.
        '            Array.Sort(groupsArray, New ListViewGroupItemSorter(sorting))
        '            Me.Groups.AddRange(groupsArray)

        '            ' Iterate through the items in myListView, assigning each 
        '            ' one to the appropriate group.
        '            Dim item As ListViewItem
        '            For Each item In Me.Items
        '                ' Retrieve the subitem text corresponding to the column.
        '                Dim subItemText As String = item.SubItems(column).Text

        '                ' For the Title column, use only the first letter.
        '                If column = 0 Then subItemText = item.Text

        '                ' Assign the item to the matching group.
        '                item.Group = CType(groups(subItemText), ListViewGroup)
        '            Next item

        '            SetWindowTheme()
        '        End Sub 'SetGroups

        '        ' Creates a Hashtable object with one entry for each unique
        '        ' subitem value (or initial letter for the parent item)
        '        ' in the specified column.
        '        Public Function CreateGroupsTable(column%) As Hashtable
        '            ' Create a Hashtable object.
        '            Dim groups As New Hashtable()

        '            ' Iterate through the items in myListView.
        '            Dim item As ListViewItem
        '            For Each item In Me.Items
        '                ' Retrieve the text value for the column.
        '                Dim subItemText As String = item.SubItems(column).Text

        '                ' Use the initial letter instead if it is the first column.
        '                If column = 0 Then subItemText = item.Text

        '                ' If the groups table does not already contain a group
        '                ' for the subItemText value, add a new group using the 
        '                ' subItemText value for the group header and Hashtable key.
        '                If Not groups.Contains(subItemText) Then
        '                    groups.Add(subItemText, New ListViewGroup(subItemText, HorizontalAlignment.Center))
        '                End If
        '            Next item

        '            ' Return the Hashtable object.
        '            Return groups
        '        End Function 'CreateGroupsTable
        '#End Region

        '#Region " ListviewGroupItemSorter "
        '        Private Class ListViewGroupItemSorter
        '            Implements IComparer

        '            Private order As SortOrder
        '            ' Stores the sort order.
        '            Friend Sub New(theOrder As SortOrder)
        '                order = theOrder
        '            End Sub 'New

        '            ' Compares the groups by header value, using the saved sort
        '            ' order to return the correct value.
        '            Friend Function Compare(x As Object, y As Object) As Integer _
        '                Implements IComparer.Compare
        '                Dim result As Integer = String.Compare( _
        '                    CType(x, ListViewGroup).Header, _
        '                    CType(y, ListViewGroup).Header)
        '                If order = SortOrder.Ascending Then
        '                    Return result
        '                Else
        '                    Return -result
        '                End If
        '            End Function 'Compare
        '        End Class 'ListViewGroupSorter 
        '#End Region

        '#End Region

#Region " LISTVIEWGROUP COMPARER "
        '-----------------------------------------------------------------------------
        ' Include all the stupid normal stuff that a ListView needs to be useful
        ' Better to use an ObjectListView!

        Overridable Sub Cpt_ListViewEx_ColumnClick(ByVal sender As System.Object, ByVal e As ColumnClickEventArgs) Handles MyBase.ColumnClick
            If e.Column = Me.lastSortColumn Then
                If Me.lastSortOrder = SortOrder.Ascending Then
                    Me.lastSortOrder = SortOrder.Descending
                Else
                    Me.lastSortOrder = SortOrder.Ascending
                End If
            Else
                Me.lastSortOrder = SortOrder.Ascending
                Me.lastSortColumn = e.Column
            End If

            Me.Rebuild()
            Me.Sort()
        End Sub

        'Private Sub listView1_ColumnClick(sender As Object, e As ColumnClickEventArgs)
        '    If e.Column = Me.lastSortColumn Then
        '        If Me.lastSortOrder = SortOrder.Ascending Then
        '            Me.lastSortOrder = SortOrder.Descending
        '        Else
        '            Me.lastSortOrder = SortOrder.Ascending
        '        End If
        '    Else
        '        Me.lastSortOrder = SortOrder.Ascending
        '        Me.lastSortColumn = e.Column
        '    End If

        '    Me.Rebuild()
        'End Sub
        Private lastSortColumn As Integer = 0
        Private lastSortOrder As SortOrder = SortOrder.Ascending

        Public Sub Rebuild()
            If Me.ShowGroups Then
                Me.BuildGroups(Me.lastSortColumn)
            Else
                'Me.ListViewItemSorter = If(lastSortOrder = SortOrder.Ascending, New ListViewAutoSorter(lastSortColumn, SortOrder.Descending), New ListViewAutoSorter(lastSortColumn, SortOrder.Ascending))
                Me.ListViewItemSorter = New ColumnComparer(Me.lastSortColumn, Me.lastSortOrder)
            End If
        End Sub

        Private Sub BuildGroups(column As Integer)
            'Dim new_sorting_column As ColumnHeader = MyBase.Columns(column)
            'Dim m_SortingColumn As ColumnHeader
            'm_SortingColumn = new_sorting_column

            'If MyBase.Columns.Cast(Of ColumnHeader).Any(Function(f) f.Text.StartsWith("<-> ") OrElse f.Text.StartsWith("<+> ")) = False Then
            '    Dim c = MyBase.Columns(0)
            '    MyBase.Columns(0).Text = "<-> " & c.Text
            'End If

            ''################## ADDED : PREFIX COLUMN HEADER ###########
            'Dim prefix$ = If(lastSortOrder = 2, "<-> ", "<+> ")
            'For Each c As ColumnHeader In MyBase.Columns
            '    If c.Text.StartsWith("<-> ") Then c.Text = c.Text.Replace("<-> ", "")
            '    If c.Text.StartsWith("<+> ") Then c.Text = c.Text.Replace("<+> ", "")
            'Next
            'm_SortingColumn.Text = If(new_sorting_column.Text.StartsWith(prefix), new_sorting_column.Text.Replace(prefix, ""), prefix & m_SortingColumn.Text)
            ''###########################################################

            Me.Groups.Clear()

            ' Getting the Count forces any internal cache of the ListView to be flushed. Without
            ' this, iterating over the Items will not work correctly if the ListView handle
            ' has not yet been created.
            Dim dummy As Integer = Me.Items.Count

            ' Separate the list view items into groups, using the group key as the descrimanent
            Dim map As New Dictionary(Of String, List(Of ListViewItem))()
            For Each lvi As ListViewItem In Me.Items
                Dim key As String = lvi.SubItems(column).Text
                'If column = 0 AndAlso key.Length > 0 Then
                '    key = key.Substring(0, 1)
                'End If
                If Not map.ContainsKey(key) Then
                    map(key) = New List(Of ListViewItem)()
                End If
                map(key).Add(lvi)
            Next

            ' Make a list of the required groups
            Dim groups As New List(Of ListViewGroup)()
            For Each key As String In map.Keys
                groups.Add(New ListViewGroup(key))
            Next

            ' Sort the groups
            groups.Sort(New ListViewGroupComparer(Me.lastSortOrder))

            ' Put each group into the list view, and give each group its member items.
            ' The order of statements is important here:
            ' - the header must be calculate before the group is added to the list view,
            '   otherwise changing the header causes a nasty redraw (even in the middle of a BeginUpdate...EndUpdate pair)
            ' - the group must be added before it is given items, otherwise an exception is thrown (is this documented?)
            Dim itemSorter As New ColumnComparer(column, Me.lastSortOrder)
            'Dim itemsorter = If(lastSortOrder = SortOrder.Ascending, New ListViewAutoSorter(lastSortColumn, SortOrder.Descending), New ListViewAutoSorter(lastSortColumn, SortOrder.Ascending))
            For Each group As ListViewGroup In groups
                Me.Groups.Add(group)
                map(group.Header).Sort(itemSorter)
                group.Items.AddRange(map(group.Header).ToArray())
            Next
        End Sub

        Friend Class ListViewGroupComparer
            Implements IComparer(Of ListViewGroup)

            Public Sub New(order As SortOrder)
                Me.sortOrder = order
            End Sub

            Private sortOrder As SortOrder

            Public Function Compare(x As ListViewGroup, y As ListViewGroup) As Integer Implements IComparer(Of ListViewGroup).Compare
                'Dim result As Integer = String.Compare(x.Header, y.Header, True)

                'If Me.sortOrder = sortOrder.Descending Then
                '    result = 0 - result
                'End If

                'Return result

                Dim l1 = DirectCast(x, ListViewGroup)
                Dim l2 = DirectCast(y, ListViewGroup)

                Dim value1 = 0.0
                Dim value2 = 0.0

                If Double.TryParse(l1.Header, value1) AndAlso Double.TryParse(l2.Header, value2) Then
                    If sortOrder = sortOrder.Ascending Then
                        Return value1.CompareTo(value2)
                    Else
                        Return value2.CompareTo(value1)
                    End If
                Else
                    Dim str1 = l1.Header
                    Dim str2 = l2.Header

                    If sortOrder = sortOrder.Ascending Then
                        Return str1.CompareTo(str2)
                    Else
                        Return str2.CompareTo(str1)
                    End If
                End If
            End Function
        End Class

        Friend Class ColumnComparer
            Implements IComparer
            Implements IComparer(Of ListViewItem)

            Public Sub New(col As Integer, order As SortOrder)
                Me.column = col
                Me.sortOrder = order
            End Sub

            Private column As Integer
            Private sortOrder As SortOrder

            Public Function Compare(x As Object, y As Object) As Integer Implements IComparer.Compare
                'Return Me.Compare(DirectCast(x, ListViewItem), DirectCast(y, ListViewItem))
                If Not (TypeOf x Is ListViewItem) Then
                    Return (0)
                End If
                If Not (TypeOf y Is ListViewItem) Then
                    Return (0)
                End If

                Dim l1 = DirectCast(x, ListViewItem)
                Dim l2 = DirectCast(y, ListViewItem)

                Dim value1 = 0.0
                Dim value2 = 0.0

                If Double.TryParse(l1.SubItems(column).Text, value1) AndAlso Double.TryParse(l2.SubItems(column).Text, value2) Then
                    If sortOrder = sortOrder.Ascending Then
                        Return value1.CompareTo(value2)
                    Else
                        Return value2.CompareTo(value1)
                    End If
                Else
                    Dim str1 = l1.SubItems(column).Text
                    Dim str2 = l2.SubItems(column).Text

                    If sortOrder = sortOrder.Ascending Then
                        Return str1.CompareTo(str2)
                    Else
                        Return str2.CompareTo(str1)
                    End If
                End If
            End Function

            Public Function Compare(x As ListViewItem, y As ListViewItem) As Integer Implements IComparer(Of ListViewItem).Compare
                Dim result As Integer = String.Compare(x.SubItems(Me.column).Text, y.SubItems(Me.column).Text, True)

                If Me.sortOrder = sortOrder.Descending Then
                    result = 0 - result
                End If

                Return result
            End Function
        End Class
#End Region

    End Class

End Namespace
