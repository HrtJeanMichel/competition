﻿
Imports System.Drawing

Namespace Extensions
    ''' <summary>
    ''' CodeProject.com "Simple pop-up control" "http://www.codeproject.com/cs/miscctrl/simplepopup.asp".
    ''' </summary>
    Friend Structure GripBounds
        Private Const GripSize As Integer = 6
        Private Const CornerGripSize As Integer = GripSize << 1

        Public Sub New(clientRectangle As Rectangle)
            Me.m_clientRectangle = clientRectangle
        End Sub

        Private m_clientRectangle As Rectangle
        Public ReadOnly Property ClientRectangle() As Rectangle
            Get
                Return m_clientRectangle
            End Get
        End Property
        'set { clientRectangle = value; }

        Public ReadOnly Property Bottom() As Rectangle
            Get
                Dim rect As Rectangle = ClientRectangle
                rect.Y = rect.Bottom - GripSize + 1
                rect.Height = GripSize
                Return rect
            End Get
        End Property

        Public ReadOnly Property BottomRight() As Rectangle
            Get
                Dim rect As Rectangle = ClientRectangle
                rect.Y = rect.Bottom - CornerGripSize + 1
                rect.Height = CornerGripSize
                rect.X = rect.Width - CornerGripSize + 1
                rect.Width = CornerGripSize
                Return rect
            End Get
        End Property

        Public ReadOnly Property Top() As Rectangle
            Get
                Dim rect As Rectangle = ClientRectangle
                rect.Height = GripSize
                Return rect
            End Get
        End Property

        Public ReadOnly Property TopRight() As Rectangle
            Get
                Dim rect As Rectangle = ClientRectangle
                rect.Height = CornerGripSize
                rect.X = rect.Width - CornerGripSize + 1
                rect.Width = CornerGripSize
                Return rect
            End Get
        End Property

        Public ReadOnly Property Left() As Rectangle
            Get
                Dim rect As Rectangle = ClientRectangle
                rect.Width = GripSize
                Return rect
            End Get
        End Property

        Public ReadOnly Property BottomLeft() As Rectangle
            Get
                Dim rect As Rectangle = ClientRectangle
                rect.Width = CornerGripSize
                rect.Y = rect.Height - CornerGripSize + 1
                rect.Height = CornerGripSize
                Return rect
            End Get
        End Property

        Public ReadOnly Property Right() As Rectangle
            Get
                Dim rect As Rectangle = ClientRectangle
                rect.X = rect.Right - GripSize + 1
                rect.Width = GripSize
                Return rect
            End Get
        End Property

        Public ReadOnly Property TopLeft() As Rectangle
            Get
                Dim rect As Rectangle = ClientRectangle
                rect.Width = CornerGripSize
                rect.Height = CornerGripSize
                Return rect
            End Get
        End Property
    End Structure
End Namespace