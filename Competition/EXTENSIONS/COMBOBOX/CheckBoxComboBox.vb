﻿
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Drawing
Imports System.Data
Imports System.Text
Imports System.Windows.Forms
Imports System.Reflection
Imports System.Collections.ObjectModel
Imports System.Collections.Specialized

Namespace Extensions
    ''' <summary>
    ''' Martin Lottering : 2007-10-27
    ''' --------------------------------
    ''' This is a usefull control in Filters. Allows you to save space and can replace a Grouped Box of CheckBoxes.
    ''' Currently used on the TasksFilter for TaskStatusses, which means the user can select which Statusses to include
    ''' in the "Search".
    ''' This control does not implement a CheckBoxListBox, instead it adds a wrapper for the normal ComboBox and Items. 
    ''' See the CheckBoxItems property.
    ''' ----------------
    ''' ALSO IMPORTANT: In Data Binding when setting the DataSource. The ValueMember must be a bool type property, because it will 
    ''' be binded to the Checked property of the displayed CheckBox. Also see the DisplayMemberSingleItem for more information.
    ''' ----------------
    ''' Extends the CodeProject PopupComboBox "Simple pop-up control" "http://www.codeproject.com/cs/miscctrl/simplepopup.asp"
    ''' by Lukasz Swiatkowski.
    ''' </summary>
    Partial Public Class CheckBoxComboBox
        Inherits PopupComboBox


        ''' <summary> 
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.IContainer = Nothing

        ''' <summary> 
        ''' Clean up any resources being used.
        ''' </summary>
        ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        Protected Overrides Sub Dispose(disposing As Boolean)
            If disposing AndAlso (components IsNot Nothing) Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

#Region "Component Designer generated code"

        ''' <summary> 
        ''' Required method for Designer support - do not modify 
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            components = New System.ComponentModel.Container()
        End Sub

#End Region

#Region "CONSTRUCTOR"

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            _CheckBoxProperties = New CheckBoxProperties()
            AddHandler _CheckBoxProperties.PropertyChanged, New EventHandler(AddressOf _CheckBoxProperties_PropertyChanged)
            ' Dumps the ListControl in a(nother) Container to ensure the ScrollBar on the ListControl does not
            ' Paint over the Size grip. Setting the Padding or Margin on the Popup or host control does
            ' not work as I expected. I don't think it can work that way.
            Dim ContainerControl As New CheckBoxComboBoxListControlContainer()
            _CheckBoxComboBoxListControl = New CheckBoxComboBoxListControl(Me)
            AddHandler _CheckBoxComboBoxListControl.Items.CheckBoxCheckedChanged, New EventHandler(AddressOf Items_CheckBoxCheckedChanged)
            ContainerControl.Controls.Add(_CheckBoxComboBoxListControl)
            ' This padding spaces neatly on the left-hand side and allows space for the size grip at the bottom.
            ContainerControl.Padding = New Padding(4, 0, 0, 14)
            ' The ListControl FILLS the ListContainer.
            _CheckBoxComboBoxListControl.Dock = DockStyle.Fill
            ' The DropDownControl used by the base class. Will be wrapped in a popup by the base class.
            DropDownControl = ContainerControl
            ' Must be set after the DropDownControl is set, since the popup is recreated.
            ' NOTE: I made the dropDown protected so that it can be accessible here. It was private.
            dropDownEx.Resizable = True
        End Sub

#End Region

#Region "PRIVATE FIELDS"

        ''' <summary>
        ''' The checkbox list control. The public CheckBoxItems property provides a direct reference to its Items.
        ''' </summary>
        Friend _CheckBoxComboBoxListControl As CheckBoxComboBoxListControl
        ''' <summary>
        ''' In DataBinding operations, this property will be used as the DisplayMember in the CheckBoxComboBoxListBox.
        ''' The normal/existing "DisplayMember" property is used by the TextBox of the ComboBox to display 
        ''' a concatenated Text of the items selected. This concatenation and its formatting however is controlled 
        ''' by the Binded object, since it owns that property.
        ''' </summary>
        Private _DisplayMemberSingleItem As String = Nothing
        Friend _MustAddHiddenItem As Boolean = False

#End Region

#Region "PRIVATE OPERATIONS"

        ''' <summary>
        ''' Builds a CSV string of the items selected.
        ''' </summary>
        Friend Function GetCSVText(skipFirstItem As Boolean) As String
            Dim ListText As String = String.Empty
            Dim StartIndex As Integer = If(DropDownStyle = ComboBoxStyle.DropDownList AndAlso DataSource Is Nothing AndAlso skipFirstItem, 1, 0)
            For Index As Integer = StartIndex To _CheckBoxComboBoxListControl.Items.Count - 1
                Dim Item As CheckBoxComboBoxItem = _CheckBoxComboBoxListControl.Items(Index)
                If Item.Checked Then
                    ListText &= If(String.IsNullOrEmpty(ListText), Item.Text, String.Format(", {0}", Item.Text))
                End If
            Next
            Return ListText
        End Function

#End Region

#Region "PUBLIC PROPERTIES"

        ''' <summary>
        ''' A direct reference to the Items of CheckBoxComboBoxListControl.
        ''' You can use it to Get or Set the Checked status of items manually if you want.
        ''' But do not manipulate the List itself directly, e.g. Adding and Removing, 
        ''' since the list is synchronised when shown with the ComboBox.Items. So for changing 
        ''' the list contents, use Items instead.
        ''' </summary>
        <Browsable(False)> _
        Public ReadOnly Property CheckBoxItems() As CheckBoxComboBoxItemList
            Get
                ' Added to ensure the CheckBoxItems are ALWAYS
                ' available for modification via code.
                If _CheckBoxComboBoxListControl.Items.Count <> Items.Count Then
                    _CheckBoxComboBoxListControl.SynchroniseControlsWithComboBoxItems()
                End If
                Return _CheckBoxComboBoxListControl.Items
            End Get
        End Property
        ''' <summary>
        ''' The DataSource of the combobox. Refreshes the CheckBox wrappers when this is set.
        ''' </summary>
        Public Shadows Property DataSource() As Object
            Get
                Return MyBase.DataSource
            End Get
            Set(value As Object)
                MyBase.DataSource = value
                If Not String.IsNullOrEmpty(ValueMember) Then
                    ' This ensures that at least the checkboxitems are available to be initialised.
                    _CheckBoxComboBoxListControl.SynchroniseControlsWithComboBoxItems()
                End If
            End Set
        End Property
        ''' <summary>
        ''' The ValueMember of the combobox. Refreshes the CheckBox wrappers when this is set.
        ''' </summary>
        Public Shadows Property ValueMember() As String
            Get
                Return MyBase.ValueMember
            End Get
            Set(value As String)
                MyBase.ValueMember = value
                If Not String.IsNullOrEmpty(ValueMember) Then
                    ' This ensures that at least the checkboxitems are available to be initialised.
                    _CheckBoxComboBoxListControl.SynchroniseControlsWithComboBoxItems()
                End If
            End Set
        End Property
        ''' <summary>
        ''' In DataBinding operations, this property will be used as the DisplayMember in the CheckBoxComboBoxListBox.
        ''' The normal/existing "DisplayMember" property is used by the TextBox of the ComboBox to display 
        ''' a concatenated Text of the items selected. This concatenation however is controlled by the Binded 
        ''' object, since it owns that property.
        ''' </summary>
        Public Property DisplayMemberSingleItem() As String
            Get
                If String.IsNullOrEmpty(_DisplayMemberSingleItem) Then
                    Return DisplayMember
                Else
                    Return _DisplayMemberSingleItem
                End If
            End Get
            Set(value As String)
                _DisplayMemberSingleItem = value
            End Set
        End Property
        ''' <summary>
        ''' Made this property Browsable again, since the Base Popup hides it. This class uses it again.
        ''' Gets an object representing the collection of the items contained in this 
        ''' System.Windows.Forms.ComboBox.
        ''' </summary>
        ''' <returns>A System.Windows.Forms.ComboBox.ObjectCollection representing the items in 
        ''' the System.Windows.Forms.ComboBox.
        ''' </returns>
        <Browsable(True)> _
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Content)> _
        Public Shadows ReadOnly Property Items() As ObjectCollection
            Get
                Return MyBase.Items
            End Get
        End Property

#End Region

#Region "EVENTS & EVENT HANDLERS"

        Public Event CheckBoxCheckedChanged As EventHandler

        Private Sub Items_CheckBoxCheckedChanged(sender As Object, e As EventArgs)
            OnCheckBoxCheckedChanged(sender, e)
        End Sub

#End Region

#Region "EVENT CALLERS and OVERRIDES e.g. OnResize()"

        Protected Sub OnCheckBoxCheckedChanged(sender As Object, e As EventArgs)
            Dim ListText As String = GetCSVText(True)
            ' The DropDownList style seems to require that the text
            ' part of the "textbox" should match a single item.
            If DropDownStyle <> ComboBoxStyle.DropDownList Then
                Text = ListText
                ' This refreshes the Text of the first item (which is not visible)
            ElseIf DataSource Is Nothing Then
                Items(0) = ListText
                ' Keep the hidden item and first checkbox item in 
                ' sync in order to ensure the Synchronise process
                ' can match the items.
                CheckBoxItems(0).ComboBoxItem = ListText
            End If
            RaiseEvent CheckBoxCheckedChanged(sender, e)
        End Sub

        ''' <summary>
        ''' Will add an invisible item when the style is DropDownList,
        ''' to help maintain the correct text in main TextBox.
        ''' </summary>
        ''' <param name="e"></param>
        Protected Overrides Sub OnDropDownStyleChanged(e As EventArgs)
            MyBase.OnDropDownStyleChanged(e)

            If DropDownStyle = ComboBoxStyle.DropDownList AndAlso DataSource Is Nothing AndAlso Not DesignMode Then
                _MustAddHiddenItem = True
            End If
        End Sub

        Protected Overrides Sub OnResize(e As EventArgs)
            ' When the ComboBox is resized, the width of the dropdown 
            ' is also resized to match the width of the ComboBox. I think it looks better.
            'Dim Size As New Size(Width, DropDownControl.Height)
            Dim Size As New Size(Width, 228)
            dropDownEx.Size = Size
            MyBase.OnResize(e)
        End Sub

#End Region

#Region "PUBLIC OPERATIONS"

        ''' <summary>
        ''' A function to clear/reset the list.
        ''' (Ubiklou : http://www.codeproject.com/KB/combobox/extending_combobox.aspx?msg=2526813#xx2526813xx)
        ''' </summary>
        Public Sub Clear()
            Me.Items.Clear()
            If DropDownStyle = ComboBoxStyle.DropDownList AndAlso DataSource Is Nothing Then
                _MustAddHiddenItem = True
            End If
        End Sub
        ''' <summary>
        ''' Uncheck all items.
        ''' </summary>
        Public Sub ClearSelection()
            For Each Item As CheckBoxComboBoxItem In CheckBoxItems
                If Item.Checked Then
                    Item.Checked = False
                End If
            Next
        End Sub

#End Region

#Region "CHECKBOX PROPERTIES (DEFAULTS)"

        Private _CheckBoxProperties As CheckBoxProperties

        ''' <summary>
        ''' The properties that will be assigned to the checkboxes as default values.
        ''' </summary>
        <Description("The properties that will be assigned to the checkboxes as default values.")> _
        <Browsable(True)> _
        Public Property CheckBoxProperties() As CheckBoxProperties
            Get
                Return _CheckBoxProperties
            End Get
            Set(value As CheckBoxProperties)
                _CheckBoxProperties = value
                _CheckBoxProperties_PropertyChanged(Me, EventArgs.Empty)
            End Set
        End Property

        Private Sub _CheckBoxProperties_PropertyChanged(sender As Object, e As EventArgs)
            For Each Item As CheckBoxComboBoxItem In CheckBoxItems
                Item.ApplyProperties(CheckBoxProperties)
            Next
        End Sub

#End Region

        Protected Overrides Sub WndProc(ByRef m As Message)
            ' 323 : Item Added
            ' 331 : Clearing
            If m.Msg = 331 AndAlso DropDownStyle = ComboBoxStyle.DropDownList AndAlso DataSource Is Nothing Then
                _MustAddHiddenItem = True
            End If

            MyBase.WndProc(m)
        End Sub
    End Class

    ''' <summary>
    ''' A container control for the ListControl to ensure the ScrollBar on the ListControl does not
    ''' Paint over the Size grip. Setting the Padding or Margin on the Popup or host control does
    ''' not work as I expected.
    ''' </summary>
    <ToolboxItem(False)> _
    Public Class CheckBoxComboBoxListControlContainer
        Inherits UserControl

#Region "CONSTRUCTOR"

        Public Sub New()
            MyBase.New()
            BackColor = SystemColors.Window
            BorderStyle = BorderStyle.FixedSingle
            AutoScaleMode = AutoScaleMode.Inherit
            ResizeRedraw = True
            ' If you don't set this, then resize operations cause an error in the base class.
            MinimumSize = New Size(1, 1)
            MaximumSize = New Size(500, 500)
        End Sub
#End Region

#Region "RESIZE OVERRIDE REQUIRED BY THE POPUP CONTROL"

        ''' <summary>
        ''' Prescribed by the Popup class to ensure Resize operations work correctly.
        ''' </summary>
        ''' <param name="m"></param>
        Protected Overrides Sub WndProc(ByRef m As Message)
            If TryCast(Parent, Popup).ProcessResizing(m) Then
                Return
            End If
            MyBase.WndProc(m)
        End Sub
#End Region

    End Class

    ''' <summary>
    ''' This ListControl that pops up to the User. It contains the CheckBoxComboBoxItems. 
    ''' The items are docked DockStyle.Top in this control.
    ''' </summary>
    <ToolboxItem(False)> _
    Public Class CheckBoxComboBoxListControl
        Inherits ScrollableControl

#Region "CONSTRUCTOR"

        Public Sub New(owner As CheckBoxComboBox)
            MyBase.New()
            DoubleBuffered = True
            _CheckBoxComboBox = owner
            _Items = New CheckBoxComboBoxItemList(_CheckBoxComboBox)
            BackColor = SystemColors.Window
            ' AutoScaleMode = AutoScaleMode.Inherit;
            AutoScroll = True
            ResizeRedraw = True
            ' if you don't set this, a Resize operation causes an error in the base class.
            MinimumSize = New Size(1, 1)
            MaximumSize = New Size(500, 500)
        End Sub

#End Region

#Region "PRIVATE PROPERTIES"

        ''' <summary>
        ''' Simply a reference to the CheckBoxComboBox.
        ''' </summary>
        Private _CheckBoxComboBox As CheckBoxComboBox
        ''' <summary>
        ''' A Typed list of ComboBoxCheckBoxItems.
        ''' </summary>
        Private _Items As CheckBoxComboBoxItemList

#End Region

        Public ReadOnly Property Items() As CheckBoxComboBoxItemList
            Get
                Return _Items
            End Get
        End Property

#Region "RESIZE OVERRIDE REQUIRED BY THE POPUP CONTROL"

        ''' <summary>
        ''' Prescribed by the Popup control to enable Resize operations.
        ''' </summary>
        ''' <param name="m"></param>
        Protected Overrides Sub WndProc(ByRef m As Message)
            If TryCast(Parent.Parent, Popup).ProcessResizing(m) Then
                Return
            End If
            MyBase.WndProc(m)
        End Sub

#End Region

#Region "PROTECTED MEMBERS"

        Protected Overrides Sub OnVisibleChanged(e As EventArgs)
            ' Synchronises the CheckBox list with the items in the ComboBox.
            SynchroniseControlsWithComboBoxItems()
            MyBase.OnVisibleChanged(e)
        End Sub
        ''' <summary>
        ''' Maintains the controls displayed in the list by keeping them in sync with the actual 
        ''' items in the combobox. (e.g. removing and adding as well as ordering)
        ''' </summary>
        Public Sub SynchroniseControlsWithComboBoxItems()
            SuspendLayout()
            If _CheckBoxComboBox._MustAddHiddenItem Then
                _CheckBoxComboBox.Items.Insert(0, _CheckBoxComboBox.GetCSVText(False))
                ' INVISIBLE ITEM
                _CheckBoxComboBox.SelectedIndex = 0
                _CheckBoxComboBox._MustAddHiddenItem = False
            End If
            Controls.Clear()
            '#Region "Disposes all items that are no longer in the combo box list"

            For Index As Integer = _Items.Count - 1 To 0 Step -1
                Dim Item As CheckBoxComboBoxItem = _Items(Index)
                If Not _CheckBoxComboBox.Items.Contains(Item.ComboBoxItem) Then
                    _Items.Remove(Item)
                    '_CheckBoxComboBox.Items.Remove(Item)
                    Item.Dispose()
                End If
            Next

            '#End Region
            '#Region "Recreate the list in the same order of the combo box items"

            Dim HasHiddenItem As Boolean = _CheckBoxComboBox.DropDownStyle = ComboBoxStyle.DropDownList AndAlso _CheckBoxComboBox.DataSource Is Nothing AndAlso Not DesignMode

            Dim NewList As New CheckBoxComboBoxItemList(_CheckBoxComboBox)
            For Index0 As Integer = 0 To _CheckBoxComboBox.Items.Count - 1
                Dim [Object] As Object = _CheckBoxComboBox.Items(Index0)
                Dim Item As CheckBoxComboBoxItem = Nothing
                ' The hidden item could match any other item when only
                ' one other item was selected.
                If Index0 = 0 AndAlso HasHiddenItem AndAlso _Items.Count > 0 Then
                    Item = _Items(0)
                Else
                    ' Skip the hidden item, it could match 
                    Dim StartIndex As Integer = If(HasHiddenItem, 1, 0)
                    For Index1 As Integer = StartIndex To _Items.Count - 1
                        If _Items(Index1).ComboBoxItem = [Object] Then
                            Item = _Items(Index1)
                            Exit For
                        End If
                    Next
                End If
                If Item Is Nothing Then
                    Item = New CheckBoxComboBoxItem(_CheckBoxComboBox, [Object])
                    Item.ApplyProperties(_CheckBoxComboBox.CheckBoxProperties)
                End If
                NewList.Add(Item)
                '_CheckBoxComboBox.Items.Add(Item)
                Item.Dock = DockStyle.Top
            Next
            _Items.Clear()
            _Items.AddRange(NewList)

            '#End Region
            '#Region "Add the items to the controls in reversed order to maintain correct docking order"

            If NewList.Count > 0 Then
                ' This reverse helps to maintain correct docking order.
                NewList.Reverse()
                ' If you get an error here that "Cannot convert to the desired 
                ' type, it probably means the controls are not binding correctly.
                ' The Checked property is binded to the ValueMember property. 
                ' It must be a bool for example.
                Controls.AddRange(NewList.ToArray())
            End If

            '#End Region

            ' Keep the first item invisible
            If _CheckBoxComboBox.DropDownStyle = ComboBoxStyle.DropDownList AndAlso _CheckBoxComboBox.DataSource Is Nothing AndAlso Not DesignMode Then
                _CheckBoxComboBox.CheckBoxItems(0).Visible = False
            End If

            ResumeLayout()
        End Sub

#End Region

    End Class

    ''' <summary>
    ''' The CheckBox items displayed in the Popup of the ComboBox.
    ''' </summary>
    <ToolboxItem(False)> _
    Public Class CheckBoxComboBoxItem
        Inherits CheckBox

#Region "CONSTRUCTOR"

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="owner">A reference to the CheckBoxComboBox.</param>
        ''' <param name="comboBoxItem">A reference to the item in the ComboBox.Items that this object is extending.</param>
        Public Sub New(owner As CheckBoxComboBox, comboBoxItem As Object)
            MyBase.New()
            DoubleBuffered = True
            _CheckBoxComboBox = owner
            _ComboBoxItem = comboBoxItem
            If _CheckBoxComboBox.DataSource IsNot Nothing Then
                AddBindings()
            Else
                Text = comboBoxItem.ToString()
            End If
        End Sub
#End Region

#Region "PRIVATE PROPERTIES"

        ''' <summary>
        ''' A reference to the CheckBoxComboBox.
        ''' </summary>
        Private _CheckBoxComboBox As CheckBoxComboBox
        ''' <summary>
        ''' A reference to the Item in ComboBox.Items that this object is extending.
        ''' </summary>
        Private _ComboBoxItem As Object

#End Region

#Region "PUBLIC PROPERTIES"

        ''' <summary>
        ''' A reference to the Item in ComboBox.Items that this object is extending.
        ''' </summary>
        Public Property ComboBoxItem() As Object
            Get
                Return _ComboBoxItem
            End Get
            Friend Set(value As Object)
                _ComboBoxItem = value
            End Set
        End Property

#End Region

#Region "BINDING HELPER"

        ''' <summary>
        ''' When using Data Binding operations via the DataSource property of the ComboBox. This
        ''' adds the required Bindings for the CheckBoxes.
        ''' </summary>
        Public Sub AddBindings()
            ' Note, the text uses "DisplayMemberSingleItem", not "DisplayMember" (unless its not assigned)
            DataBindings.Add("Text", _ComboBoxItem, _CheckBoxComboBox.DisplayMemberSingleItem)
            ' The ValueMember must be a bool type property usable by the CheckBox.Checked.
            ' This helps to maintain proper selection state in the Binded object,
            ' even when the controls are added and removed.
            DataBindings.Add("Checked", _ComboBoxItem, _CheckBoxComboBox.ValueMember, False, DataSourceUpdateMode.OnPropertyChanged, False, _
                Nothing, Nothing)
            ' Helps to maintain the Checked status of this
            ' checkbox before the control is visible
            If TypeOf _ComboBoxItem Is INotifyPropertyChanged Then
                AddHandler DirectCast(_ComboBoxItem, INotifyPropertyChanged).PropertyChanged, New PropertyChangedEventHandler(AddressOf CheckBoxComboBoxItem_PropertyChanged)
            End If
        End Sub

#End Region

#Region "PROTECTED MEMBERS"

        Protected Overrides Sub OnCheckedChanged(e As EventArgs)
            ' Found that when this event is raised, the bool value of the binded item is not yet updated.
            If _CheckBoxComboBox.DataSource IsNot Nothing Then
                Dim PI As PropertyInfo = ComboBoxItem.[GetType]().GetProperty(_CheckBoxComboBox.ValueMember)
                PI.SetValue(ComboBoxItem, Checked, Nothing)
            End If
            MyBase.OnCheckedChanged(e)
            ' Forces a refresh of the Text displayed in the main TextBox of the ComboBox,
            ' since that Text will most probably represent a concatenation of selected values.
            ' Also see DisplayMemberSingleItem on the CheckBoxComboBox for more information.
            If _CheckBoxComboBox.DataSource IsNot Nothing Then
                Dim OldDisplayMember As String = _CheckBoxComboBox.DisplayMember
                _CheckBoxComboBox.DisplayMember = Nothing
                _CheckBoxComboBox.DisplayMember = OldDisplayMember
            End If
        End Sub

#End Region

#Region "HELPER MEMBERS"

        Friend Sub ApplyProperties(properties As CheckBoxProperties)
            Me.Appearance = properties.Appearance
            Me.AutoCheck = properties.AutoCheck
            Me.AutoEllipsis = properties.AutoEllipsis
            Me.AutoSize = properties.AutoSize
            Me.CheckAlign = properties.CheckAlign
            Me.FlatAppearance.BorderColor = properties.FlatAppearanceBorderColor
            Me.FlatAppearance.BorderSize = properties.FlatAppearanceBorderSize
            Me.FlatAppearance.CheckedBackColor = properties.FlatAppearanceCheckedBackColor
            Me.FlatAppearance.MouseDownBackColor = properties.FlatAppearanceMouseDownBackColor
            Me.FlatAppearance.MouseOverBackColor = properties.FlatAppearanceMouseOverBackColor
            Me.FlatStyle = properties.FlatStyle
            Me.ForeColor = properties.ForeColor
            Me.RightToLeft = properties.RightToLeft
            Me.TextAlign = properties.TextAlign
            Me.ThreeState = properties.ThreeState
        End Sub

#End Region

#Region "EVENT HANDLERS - ComboBoxItem (DataSource)"

        ''' <summary>
        ''' Added this handler because the control doesn't seem 
        ''' to initialize correctly until shown for the first
        ''' time, which also means the summary text value
        ''' of the combo is out of sync initially.
        ''' </summary>
        Private Sub CheckBoxComboBoxItem_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
            If e.PropertyName = _CheckBoxComboBox.ValueMember Then
                Checked = CBool(_ComboBoxItem.[GetType]().GetProperty(_CheckBoxComboBox.ValueMember).GetValue(_ComboBoxItem, Nothing))
            End If
        End Sub

#End Region

    End Class

    ''' <summary>
    ''' A Typed List of the CheckBox items.
    ''' Simply a wrapper for the CheckBoxComboBox.Items. A list of CheckBoxComboBoxItem objects.
    ''' This List is automatically synchronised with the Items of the ComboBox and extended to
    ''' handle the additional boolean value. That said, do not Add or Remove using this List, 
    ''' it will be lost or regenerated from the ComboBox.Items.
    ''' </summary>
    <ToolboxItem(False)> _
    Public Class CheckBoxComboBoxItemList
        Inherits List(Of CheckBoxComboBoxItem)

#Region "CONSTRUCTORS"

        Public Sub New(checkBoxComboBox As CheckBoxComboBox)
            _CheckBoxComboBox = checkBoxComboBox
        End Sub

#End Region

#Region "PRIVATE FIELDS"

        Private _CheckBoxComboBox As CheckBoxComboBox

#End Region

#Region "EVENTS, This could be moved to the list control if needed"

        Public Event CheckBoxCheckedChanged As EventHandler

        Protected Sub OnCheckBoxCheckedChanged(sender As Object, e As EventArgs)
            RaiseEvent CheckBoxCheckedChanged(sender, e)
        End Sub
        Private Sub item_CheckedChanged(sender As Object, e As EventArgs)
            OnCheckBoxCheckedChanged(sender, e)
        End Sub

#End Region

#Region "LIST MEMBERS & OBSOLETE INDICATORS"

        <Obsolete("Do not add items to this list directly. Use the ComboBox items instead.", False)> _
        Public Shadows Sub Add(item As CheckBoxComboBoxItem)
            AddHandler item.CheckedChanged, New EventHandler(AddressOf item_CheckedChanged)
            MyBase.Add(item)
        End Sub

        Public Shadows Sub AddRange(collection As IEnumerable(Of CheckBoxComboBoxItem))
            For Each Item As CheckBoxComboBoxItem In collection
                AddHandler Item.CheckedChanged, New EventHandler(AddressOf item_CheckedChanged)
            Next
            MyBase.AddRange(collection)
        End Sub

        Public Shadows Sub Clear()
            For Each Item As CheckBoxComboBoxItem In Me
                RemoveHandler Item.CheckedChanged, AddressOf item_CheckedChanged
            Next
            MyBase.Clear()
        End Sub

        <Obsolete("Do not remove items from this list directly. Use the ComboBox items instead.", False)> _
        Public Shadows Function Remove(item As CheckBoxComboBoxItem) As Boolean
            RemoveHandler item.CheckedChanged, AddressOf item_CheckedChanged
            Return MyBase.Remove(item)
        End Function

#End Region

#Region "DEFAULT PROPERTIES"

        ''' <summary>
        ''' Returns the item with the specified displayName or Text.
        ''' </summary>
        Default Public Overloads ReadOnly Property Item(displayName As String) As CheckBoxComboBoxItem
            Get
                ' An invisible item exists in this scenario to help 
                ' with the Text displayed in the TextBox of the Combo
                ' Ubiklou : 2008-04-28 : Ignore first item. (http://www.codeproject.com/KB/combobox/extending_combobox.aspx?fid=476622&df=90&mpp=25&noise=3&sort=Position&view=Quick&select=2526813&fr=1#xx2526813xx)
                Dim StartIndex As Integer = If(_CheckBoxComboBox.DropDownStyle = ComboBoxStyle.DropDownList AndAlso _CheckBoxComboBox.DataSource Is Nothing, 1, 0)
                For Index As Integer = StartIndex To Count - 1
                    Dim It As CheckBoxComboBoxItem = Me(Index)

                    Dim Text As String
                    ' The binding might not be active yet
                    ' Ubiklou : 2008-04-28 : No databinding
                    If String.IsNullOrEmpty(It.Text) AndAlso It.DataBindings IsNot Nothing AndAlso It.DataBindings("Text") IsNot Nothing Then
                        Dim PropertyInfo As PropertyInfo = It.ComboBoxItem.[GetType]().GetProperty(It.DataBindings("Text").BindingMemberInfo.BindingMember)
                        Text = DirectCast(PropertyInfo.GetValue(It.ComboBoxItem, Nothing), String)
                    Else
                        Text = It.Text
                    End If
                    If Text.CompareTo(displayName) = 0 Then
                        Return It
                    End If
                Next
                Throw New ArgumentOutOfRangeException([String].Format("""{0}"" does not exist in this combo box.", displayName))
            End Get
        End Property

#End Region

    End Class

    <TypeConverter(GetType(ExpandableObjectConverter))> _
    Public Class CheckBoxProperties
        Public Sub New()
        End Sub

#Region "PRIVATE PROPERTIES"

        Private _Appearance As Appearance = Appearance.Normal
        Private _AutoSize As Boolean = False
        Private _AutoCheck As Boolean = True
        Private _AutoEllipsis As Boolean = False
        Private _CheckAlign As ContentAlignment = ContentAlignment.MiddleLeft
        Private _FlatAppearanceBorderColor As Color = Color.Empty
        Private _FlatAppearanceBorderSize As Integer = 1
        Private _FlatAppearanceCheckedBackColor As Color = Color.Empty
        Private _FlatAppearanceMouseDownBackColor As Color = Color.Empty
        Private _FlatAppearanceMouseOverBackColor As Color = Color.Empty
        Private _FlatStyle As FlatStyle = FlatStyle.Standard
        Private _ForeColor As Color = SystemColors.ControlText
        Private _RightToLeft As RightToLeft = RightToLeft.No
        Private _TextAlign As ContentAlignment = ContentAlignment.MiddleLeft
        Private _ThreeState As Boolean = False

#End Region

#Region "PUBLIC PROPERTIES"

        <DefaultValue(Appearance.Normal)> _
        Public Property Appearance() As Appearance
            Get
                Return _Appearance
            End Get
            Set(value As Appearance)
                _Appearance = value
                OnPropertyChanged()
            End Set
        End Property
        <DefaultValue(True)> _
        Public Property AutoCheck() As Boolean
            Get
                Return _AutoCheck
            End Get
            Set(value As Boolean)
                _AutoCheck = value
                OnPropertyChanged()
            End Set
        End Property
        <DefaultValue(False)> _
        Public Property AutoEllipsis() As Boolean
            Get
                Return _AutoEllipsis
            End Get
            Set(value As Boolean)
                _AutoEllipsis = value
                OnPropertyChanged()
            End Set
        End Property
        <DefaultValue(False)> _
        Public Property AutoSize() As Boolean
            Get
                Return _AutoSize
            End Get
            Set(value As Boolean)
                _AutoSize = True
                OnPropertyChanged()
            End Set
        End Property
        <DefaultValue(ContentAlignment.MiddleLeft)> _
        Public Property CheckAlign() As ContentAlignment
            Get
                Return _CheckAlign
            End Get
            Set(value As ContentAlignment)
                _CheckAlign = value
                OnPropertyChanged()
            End Set
        End Property
        <DefaultValue(GetType(Color), "")> _
        Public Property FlatAppearanceBorderColor() As Color
            Get
                Return _FlatAppearanceBorderColor
            End Get
            Set(value As Color)
                _FlatAppearanceBorderColor = value
                OnPropertyChanged()
            End Set
        End Property
        <DefaultValue(1)> _
        Public Property FlatAppearanceBorderSize() As Integer
            Get
                Return _FlatAppearanceBorderSize
            End Get
            Set(value As Integer)
                _FlatAppearanceBorderSize = value
                OnPropertyChanged()
            End Set
        End Property
        <DefaultValue(GetType(Color), "")> _
        Public Property FlatAppearanceCheckedBackColor() As Color
            Get
                Return _FlatAppearanceCheckedBackColor
            End Get
            Set(value As Color)
                _FlatAppearanceCheckedBackColor = value
                OnPropertyChanged()
            End Set
        End Property
        <DefaultValue(GetType(Color), "")> _
        Public Property FlatAppearanceMouseDownBackColor() As Color
            Get
                Return _FlatAppearanceMouseDownBackColor
            End Get
            Set(value As Color)
                _FlatAppearanceMouseDownBackColor = value
                OnPropertyChanged()
            End Set
        End Property
        <DefaultValue(GetType(Color), "")> _
        Public Property FlatAppearanceMouseOverBackColor() As Color
            Get
                Return _FlatAppearanceMouseOverBackColor
            End Get
            Set(value As Color)
                _FlatAppearanceMouseOverBackColor = value
                OnPropertyChanged()
            End Set
        End Property
        <DefaultValue(FlatStyle.Standard)> _
        Public Property FlatStyle() As FlatStyle
            Get
                Return _FlatStyle
            End Get
            Set(value As FlatStyle)
                _FlatStyle = value
                OnPropertyChanged()
            End Set
        End Property
        <DefaultValue(GetType(SystemColors), "ControlText")> _
        Public Property ForeColor() As Color
            Get
                Return _ForeColor
            End Get
            Set(value As Color)
                _ForeColor = value
                OnPropertyChanged()
            End Set
        End Property
        <DefaultValue(RightToLeft.No)> _
        Public Property RightToLeft() As RightToLeft
            Get
                Return _RightToLeft
            End Get
            Set(value As RightToLeft)
                _RightToLeft = value
                OnPropertyChanged()
            End Set
        End Property
        <DefaultValue(ContentAlignment.MiddleLeft)> _
        Public Property TextAlign() As ContentAlignment
            Get
                Return _TextAlign
            End Get
            Set(value As ContentAlignment)
                _TextAlign = value
                OnPropertyChanged()
            End Set
        End Property
        <DefaultValue(False)> _
        Public Property ThreeState() As Boolean
            Get
                Return _ThreeState
            End Get
            Set(value As Boolean)
                _ThreeState = value
                OnPropertyChanged()
            End Set
        End Property

#End Region

#Region "EVENTS AND EVENT CALLERS"

        ''' <summary>
        ''' Called when any property changes.
        ''' </summary>
        Public Event PropertyChanged As EventHandler

        Protected Sub OnPropertyChanged()
            RaiseEvent PropertyChanged(Me, EventArgs.Empty)
        End Sub

#End Region

    End Class
End Namespace
