﻿
Imports System.Drawing
Imports System.Runtime.InteropServices

Namespace Extensions
    ''' <summary>
    ''' CodeProject.com "Simple pop-up control" "http://www.codeproject.com/cs/miscctrl/simplepopup.asp".
    ''' </summary>
    Friend NotInheritable Class NativeMethods
        Private Sub New()
        End Sub
        Friend Const WM_NCHITTEST As Integer = &H84, WM_NCACTIVATE As Integer = &H86, WS_EX_NOACTIVATE As Integer = &H8000000, HTTRANSPARENT As Integer = -1, HTLEFT As Integer = 10, HTRIGHT As Integer = 11, _
            HTTOP As Integer = 12, HTTOPLEFT As Integer = 13, HTTOPRIGHT As Integer = 14, HTBOTTOM As Integer = 15, HTBOTTOMLEFT As Integer = 16, HTBOTTOMRIGHT As Integer = 17, _
            WM_USER As Integer = &H400, WM_REFLECT As Integer = WM_USER + &H1C00, WM_COMMAND As Integer = &H111, CBN_DROPDOWN As Integer = 7, WM_GETMINMAXINFO As Integer = &H24

        Friend Shared Function HIWORD(n As Integer) As Integer
            Return (n >> 16) And &HFFFF
        End Function

        Friend Shared Function HIWORD(n As IntPtr) As Integer
            Return HIWORD(CInt(CLng(n)))
        End Function

        Friend Shared Function LOWORD(n As Integer) As Integer
            Return n And &HFFFF
        End Function

        Friend Shared Function LOWORD(n As IntPtr) As Integer
            Return LOWORD(CInt(CLng(n)))
        End Function

        <StructLayout(LayoutKind.Sequential)> _
        Friend Structure MINMAXINFO
            Public reserved As Point
            Public maxSize As Size
            Public maxPosition As Point
            Public minTrackSize As Size
            Public maxTrackSize As Size
        End Structure
    End Class
End Namespace