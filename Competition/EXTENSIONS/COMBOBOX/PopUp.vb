﻿
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.Windows.Forms
Imports System.Security.Permissions
Imports System.Runtime.InteropServices
Imports VS = System.Windows.Forms.VisualStyles

'
'<li>Base class for custom tooltips.</li>
'<li>Office-2007-like tooltip class.</li>
'

Namespace Extensions
    ''' <summary>
    ''' CodeProject.com "Simple pop-up control" "http://www.codeproject.com/cs/miscctrl/simplepopup.asp".
    ''' Represents a pop-up window.
    ''' </summary>
    <CLSCompliant(True), ToolboxItem(False)> _
    Partial Public Class Popup
        Inherits ToolStripDropDown

#Region " Fields & Properties "

        Private m_content As Control
        ''' <summary>
        ''' Gets the content of the pop-up.
        ''' </summary>
        Public ReadOnly Property Content() As Control
            Get
                Return m_content
            End Get
        End Property

        ''' <summary>
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.IContainer = Nothing

        ''' <summary>
        ''' Clean up any resources being used.
        ''' </summary>
        ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        Protected Overrides Sub Dispose(disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If content IsNot Nothing Then
                    Dim _content As System.Windows.Forms.Control = content
                    m_content = Nothing
                    _content.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

#Region "Component Designer generated code"

        ''' <summary>
        ''' Required method for Designer support - do not modify 
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            components = New System.ComponentModel.Container()
        End Sub

#End Region


        Private fade As Boolean
        ''' <summary>
        ''' Gets a value indicating whether the <see cref="Popup"/> uses the fade effect.
        ''' </summary>
        ''' <value><c>true</c> if pop-up uses the fade effect; otherwise, <c>false</c>.</value>
        ''' <remarks>To use the fade effect, the FocusOnOpen property also has to be set to <c>true</c>.</remarks>
        Public Property UseFadeEffect() As Boolean
            Get
                Return fade
            End Get
            Set(value As Boolean)
                If fade = value Then
                    Return
                End If
                fade = value
            End Set
        End Property

        Private m_focusOnOpen As Boolean = True
        ''' <summary>
        ''' Gets or sets a value indicating whether to focus the content after the pop-up has been opened.
        ''' </summary>
        ''' <value><c>true</c> if the content should be focused after the pop-up has been opened; otherwise, <c>false</c>.</value>
        ''' <remarks>If the FocusOnOpen property is set to <c>false</c>, then pop-up cannot use the fade effect.</remarks>
        Public Property FocusOnOpen() As Boolean
            Get
                Return m_focusOnOpen
            End Get
            Set(value As Boolean)
                m_focusOnOpen = value
            End Set
        End Property

        Private m_acceptAlt As Boolean = True
        ''' <summary>
        ''' Gets or sets a value indicating whether presing the alt key should close the pop-up.
        ''' </summary>
        ''' <value><c>true</c> if presing the alt key does not close the pop-up; otherwise, <c>false</c>.</value>
        Public Property AcceptAlt() As Boolean
            Get
                Return m_acceptAlt
            End Get
            Set(value As Boolean)
                m_acceptAlt = value
            End Set
        End Property

        Private ownerPopup As Popup
        Private childPopup As Popup

        Private _resizable As Boolean
        Private m_resizable As Boolean
        ''' <summary>
        ''' Gets or sets a value indicating whether this <see cref="Popup" /> is resizable.
        ''' </summary>
        ''' <value><c>true</c> if resizable; otherwise, <c>false</c>.</value>
        Public Property Resizable() As Boolean
            Get
                Return m_resizable AndAlso _resizable
            End Get
            Set(value As Boolean)
                m_resizable = value
            End Set
        End Property

        Private host As ToolStripControlHost

        Private minSize As Size
        ''' <summary>
        ''' Gets or sets the size that is the lower limit that <see cref="M:System.Windows.Forms.Control.GetPreferredSize(System.Drawing.Size)" /> can specify.
        ''' </summary>
        ''' <returns>An ordered pair of type <see cref="T:System.Drawing.Size" /> representing the width and height of a rectangle.</returns>
        Public Shadows Property MinimumSize() As Size
            Get
                Return minSize
            End Get
            Set(value As Size)
                minSize = value
            End Set
        End Property

        Private maxSize As Size
        ''' <summary>
        ''' Gets or sets the size that is the upper limit that <see cref="M:System.Windows.Forms.Control.GetPreferredSize(System.Drawing.Size)" /> can specify.
        ''' </summary>
        ''' <returns>An ordered pair of type <see cref="T:System.Drawing.Size" /> representing the width and height of a rectangle.</returns>
        Public Shadows Property MaximumSize() As Size
            Get
                Return maxSize
            End Get
            Set(value As Size)
                maxSize = value
            End Set
        End Property

        ''' <summary>
        ''' Gets parameters of a new window.
        ''' </summary>
        ''' <returns>An object of type <see cref="T:System.Windows.Forms.CreateParams" /> used when creating a new window.</returns>
        Protected Overrides ReadOnly Property CreateParams() As CreateParams
            <SecurityPermission(SecurityAction.LinkDemand, Flags:=SecurityPermissionFlag.UnmanagedCode)> _
            Get
                Dim cp As CreateParams = MyBase.CreateParams
                cp.ExStyle = cp.ExStyle Or NativeMethods.WS_EX_NOACTIVATE
                Return cp
            End Get
        End Property

#End Region

#Region " Constructors "

        ''' <summary>
        ''' Initializes a new instance of the <see cref="Popup"/> class.
        ''' </summary>
        ''' <param name="content">The content of the pop-up.</param>
        ''' <remarks>
        ''' Pop-up will be disposed immediately after disposion of the content control.
        ''' </remarks>
        ''' <exception cref="T:System.ArgumentNullException"><paramref name="content" /> is <code>null</code>.</exception>
        Public Sub New(content As Control)
            If content Is Nothing Then
                Throw New ArgumentNullException("content")
            End If
            Me.m_content = content
            Me.fade = SystemInformation.IsMenuAnimationEnabled AndAlso SystemInformation.IsMenuFadeEnabled
            Me._resizable = True
            InitializeComponent()
            AutoSize = False
            DoubleBuffered = True
            ResizeRedraw = True
            host = New ToolStripControlHost(content)
            Padding = InlineAssignHelper(Margin, InlineAssignHelper(host.Padding, InlineAssignHelper(host.Margin, Padding.Empty)))
            MinimumSize = content.MinimumSize
            content.MinimumSize = content.Size
            MaximumSize = content.MaximumSize
            content.MaximumSize = content.Size
            Size = content.Size
            content.Location = Point.Empty
            Items.Add(host)
            AddHandler content.Disposed, Sub(sender As Object, e As EventArgs)
                                             content = Nothing
                                             Dispose(True)
                                         End Sub
            AddHandler content.RegionChanged, Sub(sender As Object, e As EventArgs) UpdateRegion()
            AddHandler content.Paint, Sub(sender As Object, e As PaintEventArgs) PaintSizeGrip(e)
            UpdateRegion()
        End Sub

#End Region

#Region " Methods "

        ''' <summary>
        ''' Processes a dialog box key.
        ''' </summary>
        ''' <param name="keyData">One of the <see cref="T:System.Windows.Forms.Keys" /> values that represents the key to process.</param>
        ''' <returns>
        ''' true if the key was processed by the control; otherwise, false.
        ''' </returns>
        Protected Overrides Function ProcessDialogKey(keyData As Keys) As Boolean
            If m_acceptAlt AndAlso ((keyData And Keys.Alt) = Keys.Alt) Then
                Return False
            End If
            Return MyBase.ProcessDialogKey(keyData)
        End Function

        ''' <summary>
        ''' Updates the pop-up region.
        ''' </summary>
        Protected Sub UpdateRegion()
            If Me.Region IsNot Nothing Then
                Me.Region.Dispose()
                Me.Region = Nothing
            End If
            If m_content.Region IsNot Nothing Then
                Me.Region = m_content.Region.Clone()
            End If
        End Sub

        ''' <summary>
        ''' Shows pop-up window below the specified control.
        ''' </summary>
        ''' <param name="control">The control below which the pop-up will be shown.</param>
        ''' <remarks>
        ''' When there is no space below the specified control, the pop-up control is shown above it.
        ''' </remarks>
        ''' <exception cref="T:System.ArgumentNullException"><paramref name="control"/> is <code>null</code>.</exception>
        Public Overloads Sub Show(control As Control)
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            SetOwnerItem(control)
            Show(control, control.ClientRectangle)
        End Sub

        ''' <summary>
        ''' Shows pop-up window below the specified area of specified control.
        ''' </summary>
        ''' <param name="control">The control used to compute screen location of specified area.</param>
        ''' <param name="area">The area of control below which the pop-up will be shown.</param>
        ''' <remarks>
        ''' When there is no space below specified area, the pop-up control is shown above it.
        ''' </remarks>
        ''' <exception cref="T:System.ArgumentNullException"><paramref name="control"/> is <code>null</code>.</exception>
        Public Overloads Sub Show(control As Control, area As Rectangle)
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            SetOwnerItem(control)
            resizableTop = InlineAssignHelper(resizableRight, False)
            Dim location As Point = control.PointToScreen(New Point(area.Left, area.Top + area.Height))
            Dim screen__1 As Rectangle = Screen.FromControl(control).WorkingArea
            If location.X + Size.Width > (screen__1.Left + screen__1.Width) Then
                resizableRight = True
                location.X = (screen__1.Left + screen__1.Width) - Size.Width
            End If
            If location.Y + Size.Height > (screen__1.Top + screen__1.Height) Then
                resizableTop = True
                location.Y -= Size.Height + area.Height
            End If
            location = control.PointToClient(location)
            MyBase.Show(control, location, ToolStripDropDownDirection.BelowRight)
        End Sub

        Private Const frames As Integer = 1
        Private Const totalduration As Integer = 0
        ' ML : 2007-11-05 : was 100 but caused a flicker.
        Private Const frameduration As Integer = totalduration \ frames
        ''' <summary>
        ''' Adjusts the size of the owner <see cref="T:System.Windows.Forms.ToolStrip" /> to accommodate the <see cref="T:System.Windows.Forms.ToolStripDropDown" /> if the owner <see cref="T:System.Windows.Forms.ToolStrip" /> is currently displayed, or clears and resets active <see cref="T:System.Windows.Forms.ToolStripDropDown" /> child controls of the <see cref="T:System.Windows.Forms.ToolStrip" /> if the <see cref="T:System.Windows.Forms.ToolStrip" /> is not currently displayed.
        ''' </summary>
        ''' <param name="visible">true if the owner <see cref="T:System.Windows.Forms.ToolStrip" /> is currently displayed; otherwise, false.</param>
        Protected Overrides Sub SetVisibleCore(visible As Boolean)
            Dim opacity__1 As Double = Opacity
            If visible AndAlso fade AndAlso m_focusOnOpen Then
                Opacity = 0
            End If
            MyBase.SetVisibleCore(visible)
            If Not visible OrElse Not fade OrElse Not m_focusOnOpen Then
                Return
            End If
            For i As Integer = 1 To frames
                If i > 1 Then
                    System.Threading.Thread.Sleep(frameduration)
                End If
                Opacity = opacity__1 * CDbl(i) / CDbl(frames)
            Next
            Opacity = opacity__1
        End Sub

        Private resizableTop As Boolean
        Private resizableRight As Boolean

        Private Sub SetOwnerItem(control As Control)
            If control Is Nothing Then
                Return
            End If
            If TypeOf control Is Popup Then
                Dim popupControl As Popup = TryCast(control, Popup)
                ownerPopup = popupControl
                ownerPopup.childPopup = Me
                OwnerItem = popupControl.Items(0)
                Return
            End If
            If control.Parent IsNot Nothing Then
                SetOwnerItem(control.Parent)
            End If
        End Sub

        ''' <summary>
        ''' Raises the <see cref="E:System.Windows.Forms.Control.SizeChanged" /> event.
        ''' </summary>
        ''' <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        Protected Overrides Sub OnSizeChanged(e As EventArgs)
            m_content.MinimumSize = Size
            m_content.MaximumSize = Size
            m_content.Size = Size
            m_content.Location = Point.Empty
            MyBase.OnSizeChanged(e)
        End Sub

        ''' <summary>
        ''' Raises the <see cref="E:System.Windows.Forms.ToolStripDropDown.Opening" /> event.
        ''' </summary>
        ''' <param name="e">A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the event data.</param>
        Protected Overrides Sub OnOpening(e As CancelEventArgs)
            If m_content.IsDisposed OrElse m_content.Disposing Then
                e.Cancel = True
                Return
            End If
            UpdateRegion()
            MyBase.OnOpening(e)
        End Sub

        ''' <summary>
        ''' Raises the <see cref="E:System.Windows.Forms.ToolStripDropDown.Opened" /> event.
        ''' </summary>
        ''' <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        Protected Overrides Sub OnOpened(e As EventArgs)
            If ownerPopup IsNot Nothing Then
                ownerPopup._resizable = False
            End If
            If m_focusOnOpen Then
                m_content.Focus()
            End If
            MyBase.OnOpened(e)
        End Sub

        Protected Overrides Sub OnClosed(e As ToolStripDropDownClosedEventArgs)
            If ownerPopup IsNot Nothing Then
                ownerPopup._resizable = True
            End If
            MyBase.OnClosed(e)
        End Sub

        Public LastClosedTimeStamp As DateTime = DateTime.Now

        Protected Overrides Sub OnVisibleChanged(e As EventArgs)
            If Visible = False Then
                LastClosedTimeStamp = DateTime.Now
            End If
            MyBase.OnVisibleChanged(e)
        End Sub

#End Region

#Region " Resizing Support "

        ''' <summary>
        ''' Processes Windows messages.
        ''' </summary>
        ''' <param name="m">The Windows <see cref="T:System.Windows.Forms.Message" /> to process.</param>
        <SecurityPermission(SecurityAction.LinkDemand, Flags:=SecurityPermissionFlag.UnmanagedCode)> _
        Protected Overrides Sub WndProc(ByRef m As Message)
            If InternalProcessResizing(m, False) Then
                Return
            End If
            MyBase.WndProc(m)
        End Sub

        ''' <summary>
        ''' Processes the resizing messages.
        ''' </summary>
        ''' <param name="m">The message.</param>
        ''' <returns>true, if the WndProc method from the base class shouldn't be invoked.</returns>
        <SecurityPermission(SecurityAction.LinkDemand, Flags:=SecurityPermissionFlag.UnmanagedCode)> _
        Public Function ProcessResizing(ByRef m As Message) As Boolean
            Return InternalProcessResizing(m, True)
        End Function

        <SecurityPermission(SecurityAction.LinkDemand, Flags:=SecurityPermissionFlag.UnmanagedCode)> _
        Private Function InternalProcessResizing(ByRef m As Message, contentControl As Boolean) As Boolean
            If m.Msg = NativeMethods.WM_NCACTIVATE AndAlso m.WParam <> IntPtr.Zero AndAlso childPopup IsNot Nothing AndAlso childPopup.Visible Then
                childPopup.Hide()
            End If
            If Not Resizable Then
                Return False
            End If
            If m.Msg = NativeMethods.WM_NCHITTEST Then
                Return OnNcHitTest(m, contentControl)
            ElseIf m.Msg = NativeMethods.WM_GETMINMAXINFO Then
                Return OnGetMinMaxInfo(m)
            End If
            Return False
        End Function

        <SecurityPermission(SecurityAction.LinkDemand, Flags:=SecurityPermissionFlag.UnmanagedCode)> _
        Private Function OnGetMinMaxInfo(ByRef m As Message) As Boolean
            Dim minmax As NativeMethods.MINMAXINFO = DirectCast(Marshal.PtrToStructure(m.LParam, GetType(NativeMethods.MINMAXINFO)), NativeMethods.MINMAXINFO)
            minmax.maxTrackSize = Me.MaximumSize
            minmax.minTrackSize = Me.MinimumSize
            Marshal.StructureToPtr(minmax, m.LParam, False)
            Return True
        End Function

        Private Function OnNcHitTest(ByRef m As Message, contentControl As Boolean) As Boolean
            Dim x As Integer = NativeMethods.LOWORD(m.LParam)
            Dim y As Integer = NativeMethods.HIWORD(m.LParam)
            Dim clientLocation As Point = PointToClient(New Point(x, y))

            Dim gripBouns As New GripBounds(If(contentControl, m_content.ClientRectangle, ClientRectangle))
            Dim transparent As New IntPtr(NativeMethods.HTTRANSPARENT)

            If resizableTop Then
                If resizableRight AndAlso gripBouns.TopLeft.Contains(clientLocation) Then
                    m.Result = If(contentControl, transparent, CType(NativeMethods.HTTOPLEFT, IntPtr))
                    Return True
                End If
                If Not resizableRight AndAlso gripBouns.TopRight.Contains(clientLocation) Then
                    m.Result = If(contentControl, transparent, CType(NativeMethods.HTTOPRIGHT, IntPtr))
                    Return True
                End If
                If gripBouns.Top.Contains(clientLocation) Then
                    m.Result = If(contentControl, transparent, CType(NativeMethods.HTTOP, IntPtr))
                    Return True
                End If
            Else
                If resizableRight AndAlso gripBouns.BottomLeft.Contains(clientLocation) Then
                    m.Result = If(contentControl, transparent, CType(NativeMethods.HTBOTTOMLEFT, IntPtr))
                    Return True
                End If
                If Not resizableRight AndAlso gripBouns.BottomRight.Contains(clientLocation) Then
                    m.Result = If(contentControl, transparent, CType(NativeMethods.HTBOTTOMRIGHT, IntPtr))
                    Return True
                End If
                If gripBouns.Bottom.Contains(clientLocation) Then
                    m.Result = If(contentControl, transparent, CType(NativeMethods.HTBOTTOM, IntPtr))
                    Return True
                End If
            End If
            If resizableRight AndAlso gripBouns.Left.Contains(clientLocation) Then
                m.Result = If(contentControl, transparent, CType(NativeMethods.HTLEFT, IntPtr))
                Return True
            End If
            If Not resizableRight AndAlso gripBouns.Right.Contains(clientLocation) Then
                m.Result = If(contentControl, transparent, CType(NativeMethods.HTRIGHT, IntPtr))
                Return True
            End If
            Return False
        End Function

        Private sizeGripRenderer As VS.VisualStyleRenderer
        ''' <summary>
        ''' Paints the size grip.
        ''' </summary>
        ''' <param name="e">The <see cref="System.Windows.Forms.PaintEventArgs" /> instance containing the event data.</param>
        Public Sub PaintSizeGrip(e As PaintEventArgs)
            If e Is Nothing OrElse e.Graphics Is Nothing OrElse Not m_resizable Then
                Return
            End If
            Dim clientSize As Size = m_content.ClientSize
            If Application.RenderWithVisualStyles Then
                If Me.sizeGripRenderer Is Nothing Then
                    Me.sizeGripRenderer = New VS.VisualStyleRenderer(VS.VisualStyleElement.Status.Gripper.Normal)
                End If
                Me.sizeGripRenderer.DrawBackground(e.Graphics, New Rectangle(clientSize.Width - &H10, clientSize.Height - &H10, &H10, &H10))
            Else
                ControlPaint.DrawSizeGrip(e.Graphics, m_content.BackColor, clientSize.Width - &H10, clientSize.Height - &H10, &H10, &H10)
            End If
        End Sub
        Private Shared Function InlineAssignHelper(Of T)(ByRef target As T, value As T) As T
            target = value
            Return value
        End Function

#End Region

    End Class
End Namespace