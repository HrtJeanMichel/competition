﻿
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.Windows.Forms
Imports System.Security.Permissions

Namespace Extensions
    ''' <summary>
    ''' CodeProject.com "Simple pop-up control" "http://www.codeproject.com/cs/miscctrl/simplepopup.asp".
    ''' Represents a Windows combo box control with a custom popup control attached.
    ''' </summary>
    <ToolboxBitmap(GetType(System.Windows.Forms.ComboBox)), ToolboxItem(True), ToolboxItemFilter("System.Windows.Forms"), Description("Displays an editable text box with a drop-down list of permitted values.")> _
    Partial Public Class PopupComboBox
        Inherits ComboBox

        ''' <summary>
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.IContainer = Nothing

        ''' <summary>
        ''' Clean up any resources being used.
        ''' </summary>
        ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        Protected Overrides Sub Dispose(disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If dropDownEx IsNot Nothing Then
                    dropDownEx.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

#Region "Component Designer generated code"

        ''' <summary>
        ''' Required method for Designer support - do not modify 
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            Me.SuspendLayout()
            ' 
            ' PopupComboBox
            ' 
            Me.ResumeLayout(False)

        End Sub

#End Region


        ''' <summary>
        ''' Initializes a new instance of the <see cref="PopupComboBox" /> class.
        ''' </summary>
        Public Sub New()
            InitializeComponent()
            MyBase.DropDownHeight = InlineAssignHelper(MyBase.DropDownWidth, 1)
            MyBase.IntegralHeight = False
        End Sub

        ''' <summary>
        ''' The pop-up wrapper for the dropDownControl. 
        ''' Made PROTECTED instead of PRIVATE so descendent classes can set its Resizable property.
        ''' Note however the pop-up properties must be set after the dropDownControl is assigned, since this 
        ''' popup wrapper is recreated when the dropDownControl is assigned.
        ''' </summary>
        Protected dropDownEx As Popup

        Private m_dropDownControl As Control
        ''' <summary>
        ''' Gets or sets the drop down control.
        ''' </summary>
        ''' <value>The drop down control.</value>
        Public Property DropDownControl() As Control
            Get
                Return m_dropDownControl
            End Get
            Set(value As Control)
                If m_dropDownControl Is value Then
                    Return
                End If
                m_dropDownControl = value
                dropDownEx = New Popup(value)
            End Set
        End Property

        ''' <summary>
        ''' Shows the drop down.
        ''' </summary>
        Public Sub ShowDropDown()
            If dropDownEx IsNot Nothing Then
                dropDownEx.Show(Me)
            End If
        End Sub

        ''' <summary>
        ''' Hides the drop down.
        ''' </summary>
        Public Sub HideDropDown()
            If dropDownEx IsNot Nothing Then
                dropDownEx.Hide()
            End If
        End Sub

        ''' <summary>
        ''' Processes Windows messages.
        ''' </summary>
        ''' <param name="m">The Windows <see cref="T:System.Windows.Forms.Message" /> to process.</param>
        <SecurityPermission(SecurityAction.LinkDemand, Flags:=SecurityPermissionFlag.UnmanagedCode)> _
        Protected Overrides Sub WndProc(ByRef m As Message)
            If m.Msg = (NativeMethods.WM_REFLECT + NativeMethods.WM_COMMAND) Then
                If NativeMethods.HIWORD(m.WParam) = NativeMethods.CBN_DROPDOWN Then
                    ' Blocks a redisplay when the user closes the control by clicking 
                    ' on the combobox.
                    Dim TimeSpan As TimeSpan = DateTime.Now.Subtract(dropDownEx.LastClosedTimeStamp)
                    If TimeSpan.TotalMilliseconds > 500 Then
                        ShowDropDown()
                    End If
                    Return
                End If
            End If
            MyBase.WndProc(m)
        End Sub

#Region " Unused Properties "

        ''' <summary>This property is not relevant for this class.</summary>
        ''' <returns>This property is not relevant for this class.</returns>
        <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)> _
        Public Shadows Property DropDownWidth() As Integer
            Get
                Return MyBase.DropDownWidth
            End Get
            Set(value As Integer)
                MyBase.DropDownWidth = value
            End Set
        End Property

        ''' <summary>This property is not relevant for this class.</summary>
        ''' <returns>This property is not relevant for this class.</returns>
        <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)> _
        Public Shadows Property DropDownHeight() As Integer
            Get
                Return MyBase.DropDownHeight
            End Get
            Set(value As Integer)
                dropDownEx.Height = value
                MyBase.DropDownHeight = value
            End Set
        End Property

        ''' <summary>This property is not relevant for this class.</summary>
        ''' <returns>This property is not relevant for this class.</returns>
        <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)> _
        Public Shadows Property IntegralHeight() As Boolean
            Get
                Return MyBase.IntegralHeight
            End Get
            Set(value As Boolean)
                MyBase.IntegralHeight = value
            End Set
        End Property

        ''' <summary>This property is not relevant for this class.</summary>
        ''' <returns>This property is not relevant for this class.</returns>
        <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)> _
        Public Shadows ReadOnly Property Items() As ObjectCollection
            Get
                Return MyBase.Items
            End Get
        End Property

        ''' <summary>This property is not relevant for this class.</summary>
        ''' <returns>This property is not relevant for this class.</returns>
        <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)> _
        Public Shadows Property ItemHeight() As Integer
            Get
                Return MyBase.ItemHeight
            End Get
            Set(value As Integer)
                MyBase.ItemHeight = value
            End Set
        End Property
        Private Shared Function InlineAssignHelper(Of T)(ByRef target As T, value As T) As T
            target = value
            Return value
        End Function

#End Region

    End Class
End Namespace
