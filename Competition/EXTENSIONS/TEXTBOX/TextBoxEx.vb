﻿Imports System
Imports System.ComponentModel
Imports System.Drawing
Imports System.Text
Imports System.Windows.Forms
Imports System.Text.RegularExpressions

Namespace Extensions
    Public Class TextBoxEx
        Inherits TextBox

#Region " Fields "
        Private m_errorProvider As ErrorProvider = Nothing
        Private m_useRegularExpressionRegex As Regex = Nothing
#End Region

#Region " Events "
        Public Event CharsUnauthorized As CharsUnauthorizedEventHandler
#End Region

#Region " Properties "
        Private _UseRegularExpressionPattern As String
        Public Property UseRegularExpressionPattern() As String
            Get
                Return _UseRegularExpressionPattern
            End Get
            Set(ByVal value As String)
                If value IsNot Nothing Then m_useRegularExpressionRegex = New Regex(value, RegexOptions.Compiled)
                _UseRegularExpressionPattern = value
            End Set
        End Property

        Public Property UseRegularExpression As Boolean
        Public Property UseRegularExpressionErrorMessage As String
#End Region

#Region " Constructor "
        Public Sub New()
            m_errorProvider = New ErrorProvider
            'm_errorProvider.SetError(Me, "Ce champs ne peut être vide !")
        End Sub
#End Region

#Region " Methods "

        Protected Overrides Sub OnValidating(e As CancelEventArgs)
            'OnValidating est déclenché lorsqu'on clique sur un autre contrôle dans la zone cliente du formulaire
            MyBase.OnValidating(e)
            SetErrorProvider()
        End Sub

        Protected Overrides Sub OnTextChanged(e As EventArgs)
            'OnTextChanged est déclenché lorsqu'un caractère est saisi dans la textbox.
            MyBase.OnTextChanged(e)
            SetErrorProvider()
        End Sub

        Private Sub SetErrorProvider()
            If UseRegularExpression And UseRegularExpressionPattern IsNot Nothing Then
                If Me.Text.StartsWith(" ") Then
                    Me.Text = Me.Text.Substring(0, Me.Text.Length - 1)
                    Me.Select(Me.Text.Length, 0)
                    'm_errorProvider.SetError(Me, "Ce champs ne peut pas être vide !")
                    RaiseEvent CharsUnauthorized(Me, New TextBoxExEventArgs(TextBoxExEventArgs.Allowed.No))
                Else
                    'On appelle la Fonction qui permet de vérifier l'exactitude des caractères saisis dans la textbox 
                    If ValidateControl() = False Then
                        'Me.Text = String.Empty
                        If Not Me.Text.Length = 0 Then
                            Me.Text = Me.Text.Substring(0, Me.Text.Length - 1)
                            Me.Select(Me.Text.Length, 0)
                        End If
                        'ErrorProvider affiche une infobulle rouge à droite de la textbox.
                        m_errorProvider.SetError(Me, UseRegularExpressionErrorMessage)
                        RaiseEvent CharsUnauthorized(Me, New TextBoxExEventArgs(TextBoxExEventArgs.Allowed.No))
                    Else
                        'La saisie est valide donc rien n'est affiché dans ErrorProvider
                        m_errorProvider.SetError(Me, String.Empty)
                        RaiseEvent CharsUnauthorized(Me, New TextBoxExEventArgs(TextBoxExEventArgs.Allowed.Yes))
                    End If
                End If
            End If
        End Sub

        Private Function ValidateControl() As Boolean
            ' Si le pattern est bon alors on vérifie que le texte tapé correspond au pattern 
            Return m_useRegularExpressionRegex.IsMatch(Me.Text)
        End Function
#End Region

    End Class
End Namespace


