﻿
Namespace Extensions

#Region " Delegates "
    Public Delegate Sub CharsUnauthorizedEventHandler(sender As Object, e As TextBoxExEventArgs)
#End Region

    Public Class TextBoxExEventArgs
        Inherits EventArgs

#Region " Enums "
        Public Enum Allowed
            No = 0
            Yes = 1
        End Enum
#End Region

#Region " Constructor "
        Public Sub New(allow As Allowed)
            _CharsAllowed = If(allow = Allowed.No, False, True)
        End Sub
#End Region

#Region " Properties "
        Property CharsAllowed As Boolean
#End Region

    End Class
End Namespace
