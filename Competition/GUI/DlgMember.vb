﻿Imports System.Windows.Forms
Imports Competition.Settings
Imports Competition.Core

Public Class DlgMember

#Region " Fields "
    Private m_Param As AppSettings
    Private m_Index As Integer
    Private m_Modify As Boolean
#End Region

#Region " Properties "
    Public Property CurrentRace As Race
    Public Property CurrentMember As Member
    Public Property NewMember As Member
#End Region

#Region " Constructor "
    Sub New(Param As AppSettings, Race As Race, Index As Integer)
        InitializeComponent()
        m_Param = Param
        CurrentRace = Race
        NewMember = New Member
        m_Index = Index
        TxbBibNumber.Text = Index.ToString
        For Each cat In CurrentRace.Categories
            CbxCategory.Items.Add(cat.Value)
        Next
    End Sub

    Sub New(Param As AppSettings, Race As Race, CurrMember As Member)
        InitializeComponent()
        m_Param = Param
        CurrentRace = Race
        m_Modify = True
        For Each cat In CurrentRace.Categories
            CbxCategory.Items.Add(cat.Value)
        Next
        Me.Text = "Modifier un participant"
        Me.BtnValidate.Text = "Modifier"
        NewMember = New Member
        CurrentMember = CurrMember

        With CurrentMember
            TxbBibNumber.Text = .Bib
            m_Index = .Bib
            TxbName.Text = .Name
            TxbFirstName.Text = .FirstName
            CbxSex.SelectedItem = .Sex
            CbxCategory.SelectedItem = .Category
            TxbBirthDate.Text = .BirthDate
            TxbClub.Text = .Club
            TxbComment.Text = .Comment
        End With
    End Sub

#End Region

#Region " Methods "

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub BtnValidate_Click(sender As Object, e As EventArgs) Handles BtnValidate.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        With NewMember
            .Bib = m_Index.ToString
            .Name = TxbName.Text
            .FirstName = TxbFirstName.Text
            .Sex = CbxSex.SelectedItem
            .Category = CbxCategory.SelectedItem
            .BirthDate = TxbBirthDate.Text
            .Club = TxbClub.Text
            .Comment = TxbComment.Text
        End With

        If Not CurrentMember Is Nothing Then
            CurrentRace.Members.Remove(CurrentMember)
        End If

        CurrentRace.Members.Add(NewMember)
        m_Param.SaveFile()
        Me.Close()
    End Sub

    Private Sub DtpDate_ValueChanged(sender As Object, e As EventArgs) Handles DtpBirthDate.ValueChanged
        TxbBirthDate.Text = String.Format("{0:MM/dd/yyyy}", DtpBirthDate.Value)
    End Sub

    Private Sub TxbName_TextChanged(sender As Object, e As EventArgs) Handles TxbName.TextChanged
        Dim Enabled = TxbName.Text.Length > 0
        BtnValidate.Enabled = Enabled
    End Sub

#End Region

End Class
