﻿Imports System.Timers
Imports Competition.Settings
Imports System.IO
Imports Competition.Core
Imports Competition.Extensions

Public Class FrmMain

#Region " Fields "
    Friend WithEvents Chrono As Chrono
    Friend Bdd As AppSettings
    Friend Ongoing As Boolean
    Friend Editors As Control()
    Friend CurrentRace As Race
#End Region

#Region " Constructor "
    Public Sub New()
        InitializeComponent()
        Editors = New Control() {Nothing, Nothing, TxbEditor}
        Chrono = New Chrono
        Bdd = New AppSettings
        If Not File.Exists(Application.StartupPath & "\CompetitionBDD.xml") Then
            Bdd.SaveFile()
        Else
            Bdd = AppSettings.LoadFile()
        End If
        Bdd.Frm = Me
        AddHandler Me.KeyUp, New KeyEventHandler(AddressOf KeyPressed)
    End Sub
#End Region

#Region " Methods "

#Region " FRM_MAIN "

    Private Sub FrmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Bdd.LoadRaces()
        LoadColumns()
    End Sub

    Private Sub FrmMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If Ongoing Then e.Cancel = True
    End Sub

#End Region

#Region " CHRONO "
    Private Sub m_Timer_Tick(sender As Object, e As TimerElapsedEventArgs) Handles Chrono.TimerElapsed
        Try
            LblElapsed.Text = e.Elapsed
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub BtnStartStop_Click(sender As Object, e As EventArgs) Handles BtnStartStop.Click
        Select Case BtnStartStop.Tag
            Case "Démarrer"
                CurrentRace.Start()
            Case "Arrêter"
                If MessageBox.Show("Etes-vous certains de vouloir arrêter la course ?", "Fin", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    CurrentRace.Stop()
                End If
        End Select
    End Sub

#End Region

#Region " RACES "

    Private Sub LvRaces_ItemSelectionChanged(sender As Object, e As ListViewItemSelectionChangedEventArgs) Handles LvRaces.ItemSelectionChanged
        If e.IsSelected Then
            CurrentRace = TryCast(e.Item.Tag, Race)
            CurrentRace.Frm = Me
            CurrentRace.Load()
        Else
            TryCast(e.Item.Tag, Race).UnLoad()
            CurrentRace = Nothing
        End If
    End Sub

    Private Sub TsBtnRaceAdd_Click(sender As Object, e As EventArgs) Handles TsBtnRaceAdd.Click
        Using DlgNew As New DlgRace(Bdd)
            If DlgNew.ShowDialog = Windows.Forms.DialogResult.OK Then
                With DlgNew
                    Bdd.LoadRaces()
                    .NewRace.Select()
                End With
            End If
        End Using
    End Sub

    Private Sub TsBtnRaceRemove_Click(sender As Object, e As EventArgs) Handles TsBtnRaceRemove.Click
        If MessageBox.Show("Etes-vous certains de vouloir supprimer cette course ?", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            If Not LvRaces.SelectedItems.Count = 0 Then
                Bdd.DeleteRace()
                Bdd.LoadRaces()
            End If
        End If

    End Sub

    Private Sub TsBtnRaceModify_Click(sender As Object, e As EventArgs) Handles TsBtnRaceModify.Click, LvRaces.DoubleClick
        If Not CurrentRace Is Nothing Then
            Using DlgNew As New DlgRace(Bdd, CurrentRace)
                If DlgNew.ShowDialog = Windows.Forms.DialogResult.OK Then
                    With DlgNew
                        Bdd.LoadRaces()
                        .NewRace.Select()
                    End With
                End If
            End Using
        End If
    End Sub

#End Region

#Region " MEMBERS "

    Private Sub LvMembers_ItemSelectionChanged(sender As Object, e As ListViewItemSelectionChangedEventArgs) Handles LvMembers.ItemSelectionChanged
        If LvMembers.SelectedItems.Count > 1 Then
            TsBtnMemberModify.Enabled = False
        ElseIf LvMembers.SelectedItems.Count = 0 Then
            TsBtnMemberRemove.Enabled = False
            TsBtnMemberModify.Enabled = False
        ElseIf LvMembers.SelectedItems.Count = 1 Then
            TsBtnMemberRemove.Enabled = True
            TsBtnMemberModify.Enabled = True
        End If
    End Sub

    Private Sub TsBtnMemberAdd_Click(sender As Object, e As EventArgs) Handles TsBtnMemberAdd.Click
        Using dlg As New DlgMember(Bdd, CurrentRace, MemberNextIndex)
            If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
                With dlg
                    Bdd.LoadRaces()
                    .CurrentRace.Select()
                End With
            End If
        End Using
    End Sub

    Private Function MemberNextIndex() As Integer
        Dim index% = LvMembers.Items.Count
        Dim NotFound As Boolean = False
        Dim id% = 1
        For Each it As ListViewItem In LvMembers.Items
            Dim found = LvMembers.Items.Cast(Of ListViewItem).FirstOrDefault(Function(f) f.Text = id.ToString)
            If found Is Nothing Then
                NotFound = True
                Exit For
            End If
            id += 1
        Next
        Return If(NotFound = False, index + 1, id)
    End Function

    Private Sub TsBtnMemberRemove_Click(sender As Object, e As EventArgs) Handles TsBtnMemberRemove.Click
        If MessageBox.Show("Etes-vous certains de vouloir supprimer " & If(LvMembers.SelectedItems.Count > 1, "ces participants", "ce participant") & " ?", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            If Not LvMembers.SelectedItems.Count = 0 Then
                CurrentRace.DeleteMembers()
                CurrentRace.Select()
            End If
        End If
    End Sub

    Private Sub TsBtnMemberModify_Click(sender As Object, e As EventArgs) Handles TsBtnMemberModify.Click, LvMembers.DoubleClick
        If LvMembers.SelectedItems.Count = 1 Then
            Using dlg As New DlgMember(Bdd, CurrentRace, TryCast(LvMembers.SelectedItems.Item(0).Tag, Member))
                If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
                    With dlg
                        Bdd.LoadRaces()
                        .CurrentRace.Select()
                    End With
                End If
            End Using
        End If
    End Sub

#End Region

#Region " RANKING "

    Private Sub BtnNewArriving_Click(sender As Object, e As EventArgs) Handles BtnNewArriving.Click
        CurrentRace.AddArriving()
    End Sub

    Private Sub KeyPressed(sender As Object, e As KeyEventArgs)
        If Chrono.TimerOnGoing Then
            If e.KeyCode = Keys.F1 Then CurrentRace.AddArriving()
        End If
    End Sub

    Private Sub LvArrived_ItemSelectionChanged(sender As Object, e As ListViewItemSelectionChangedEventArgs) Handles LvArrived.ItemSelectionChanged
        If LvArrived.SelectedItems.Count <> 0 Then
            If Not CurrentRace Is Nothing Then
                If LvArrived.Items.Count >= CurrentRace.Members.Max(Function(f) f.Bib) Then BtnNewArriving.Enabled = False
            End If
        End If
    End Sub

    Private Sub LvArrived_SubItemClicked(sender As Object, e As SubItemEventArgs) Handles LvArrived.SubItemClicked
        If (e.SubItem = 2) Then LvArrived.StartEditing(Editors(e.SubItem), e.Item, e.SubItem)
    End Sub

    Private Sub LvArrived_SubItemEndEditing(sender As Object, e As SubItemEndEditingEventArgs) Handles LvArrived.SubItemEndEditing
        If (e.SubItem = 2) Then
            Dim txb = TryCast(Editors(e.SubItem), TextBoxEx)
            If txb.Text.Length > 0 Then
                Dim Exists = LvArrived.Items.Cast(Of ListViewItem).Any(Function(f) f.SubItems(2).Text = txb.Text)
                If Exists Then
                    e.DisplayText = ""
                    LvArrived.StartEditing(Editors(e.SubItem), e.Item, e.SubItem)
                    e.Cancel = True
                Else
                    Dim MaxSeq = CurrentRace.Members.Max(Function(f) f.Bib)
                    If CInt(txb.Text) > MaxSeq Then
                        e.DisplayText = ""
                        LvArrived.StartEditing(Editors(e.SubItem), e.Item, e.SubItem)
                        e.Cancel = True
                    Else
                        Dim member = CurrentRace.Members.FirstOrDefault(Function(f) f.Bib = CInt(txb.Text))
                        If Not member Is Nothing Then
                            With member
                                e.Item.SubItems(2).Text = CStr(.Bib)
                                e.Item.SubItems(3).Text = .Name
                                e.Item.SubItems(4).Text = .FirstName
                                e.Item.SubItems(5).Text = .Sex
                                e.Item.SubItems(6).Text = .Category
                                e.Item.SubItems(7).Text = .BirthDate
                                e.Item.SubItems(8).Text = .Club
                                e.Item.SubItems(9).Text = .Comment
                            End With

                            MaxSeq = CurrentRace.Members.Count
                            If LvArrived.Items.Count >= MaxSeq Then
                                If LvArrived.Items.Cast(Of ListViewItem).Any(Function(f) f.SubItems(2).Text = "") = False Then CurrentRace.Stop()
                            End If
                        Else
                            e.DisplayText = ""
                            LvArrived.StartEditing(Editors(e.SubItem), e.Item, e.SubItem)
                            e.Cancel = True
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub TsMiBtnPrint_Click(sender As Object, e As EventArgs) Handles TsMiBtnPrint.Click
        LvArrived.Print()
    End Sub

    Private Sub TsMiBtnPreview_Click(sender As Object, e As EventArgs) Handles TsMiBtnPreview.Click
        LvArrived.Title = "Aperçu des résultats de la course : " & LblTitle.Text
        LvArrived.FitToPage = True
        LvArrived.PrintPreview()
    End Sub

    Private Sub TsMiBtnSetup_Click(sender As Object, e As EventArgs) Handles TsMiBtnSetup.Click
        LvArrived.PageSetup()
    End Sub

    Private Sub LoadColumns()
        For i% = 0 To CbxArrivedHideColumns.CheckBoxItems.Count - 1
            CbxArrivedHideColumns.CheckBoxItems(i).Checked = True
            AddHandler CbxArrivedHideColumns.CheckBoxItems(i).CheckedChanged, AddressOf CbxCheckboxes
        Next
    End Sub

    Private Sub CbxCheckboxes(sender As Object, e As EventArgs)
        Dim chb As CheckBox = TryCast(sender, CheckBox)
        HideColumn(chb, If(chb.Checked, False, True))
    End Sub

    Private Sub HideColumn(c As CheckBox, Hide As Boolean)
        Select Case c.Text
            Case "Place"
                ClhArrivedPlace.Width = If(Hide, 0, 47)
            Case "Chrono"
                ClhArrivedChrono.Width = If(Hide, 0, 80)
            Case "Dossard"
                ClhArrivedDossard.Width = If(Hide, 0, 64)
            Case "Nom"
                ClhArrivedNom.Width = If(Hide, 0, 132)
            Case "Prénom"
                ClhArrivedPrenom.Width = If(Hide, 0, 120)
            Case "Sexe"
                ClhArrivedSexe.Width = If(Hide, 0, 63)
            Case "Catégorie"
                ClhArrivedCategorie.Width = If(Hide, 0, 89)
            Case "Date de naissance"
                ClhArrivedDateNaissance.Width = If(Hide, 0, 125)
            Case "Club"
                ClhArrivedClub.Width = If(Hide, 0, 158)
        End Select
    End Sub

#End Region

#End Region

End Class
