﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DlgMember
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.TxbComment = New System.Windows.Forms.TextBox()
        Me.LblComment = New System.Windows.Forms.Label()
        Me.TxbClub = New Competition.Extensions.TextBoxEx()
        Me.LblPlace = New System.Windows.Forms.Label()
        Me.TxbBirthDate = New System.Windows.Forms.MaskedTextBox()
        Me.LblBirthDate = New System.Windows.Forms.Label()
        Me.DtpBirthDate = New System.Windows.Forms.DateTimePicker()
        Me.CbxCategory = New System.Windows.Forms.ComboBox()
        Me.LblCategory = New System.Windows.Forms.Label()
        Me.CbxSex = New System.Windows.Forms.ComboBox()
        Me.LblSex = New System.Windows.Forms.Label()
        Me.TxbFirstName = New Competition.Extensions.TextBoxEx()
        Me.LblFirstName = New System.Windows.Forms.Label()
        Me.TxbName = New Competition.Extensions.TextBoxEx()
        Me.TxbBibNumber = New Competition.Extensions.TextBoxEx()
        Me.LblName = New System.Windows.Forms.Label()
        Me.LblBibNumber = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.BtnValidate = New System.Windows.Forms.Button()
        Me.BtnCancel = New System.Windows.Forms.Button()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.TxbComment)
        Me.SplitContainer1.Panel1.Controls.Add(Me.LblComment)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TxbClub)
        Me.SplitContainer1.Panel1.Controls.Add(Me.LblPlace)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TxbBirthDate)
        Me.SplitContainer1.Panel1.Controls.Add(Me.LblBirthDate)
        Me.SplitContainer1.Panel1.Controls.Add(Me.DtpBirthDate)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CbxCategory)
        Me.SplitContainer1.Panel1.Controls.Add(Me.LblCategory)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CbxSex)
        Me.SplitContainer1.Panel1.Controls.Add(Me.LblSex)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TxbFirstName)
        Me.SplitContainer1.Panel1.Controls.Add(Me.LblFirstName)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TxbName)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TxbBibNumber)
        Me.SplitContainer1.Panel1.Controls.Add(Me.LblName)
        Me.SplitContainer1.Panel1.Controls.Add(Me.LblBibNumber)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.TableLayoutPanel1)
        Me.SplitContainer1.Size = New System.Drawing.Size(274, 507)
        Me.SplitContainer1.SplitterDistance = 462
        Me.SplitContainer1.TabIndex = 0
        '
        'TxbComment
        '
        Me.TxbComment.Location = New System.Drawing.Point(26, 397)
        Me.TxbComment.Multiline = True
        Me.TxbComment.Name = "TxbComment"
        Me.TxbComment.Size = New System.Drawing.Size(224, 50)
        Me.TxbComment.TabIndex = 34
        '
        'LblComment
        '
        Me.LblComment.AutoSize = True
        Me.LblComment.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.LblComment.Location = New System.Drawing.Point(25, 381)
        Me.LblComment.Name = "LblComment"
        Me.LblComment.Size = New System.Drawing.Size(74, 13)
        Me.LblComment.TabIndex = 35
        Me.LblComment.Text = "Commentaire :"
        '
        'TxbClub
        '
        Me.TxbClub.Location = New System.Drawing.Point(28, 344)
        Me.TxbClub.Name = "TxbClub"
        Me.TxbClub.Size = New System.Drawing.Size(195, 20)
        Me.TxbClub.TabIndex = 33
        Me.TxbClub.UseRegularExpression = False
        Me.TxbClub.UseRegularExpressionErrorMessage = Nothing
        Me.TxbClub.UseRegularExpressionPattern = Nothing
        '
        'LblPlace
        '
        Me.LblPlace.AutoSize = True
        Me.LblPlace.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.LblPlace.Location = New System.Drawing.Point(25, 328)
        Me.LblPlace.Name = "LblPlace"
        Me.LblPlace.Size = New System.Drawing.Size(34, 13)
        Me.LblPlace.TabIndex = 32
        Me.LblPlace.Text = "Club :"
        '
        'TxbBirthDate
        '
        Me.TxbBirthDate.BackColor = System.Drawing.Color.White
        Me.TxbBirthDate.Location = New System.Drawing.Point(28, 291)
        Me.TxbBirthDate.Mask = "00/00/0000"
        Me.TxbBirthDate.Name = "TxbBirthDate"
        Me.TxbBirthDate.RejectInputOnFirstFailure = True
        Me.TxbBirthDate.Size = New System.Drawing.Size(106, 20)
        Me.TxbBirthDate.TabIndex = 31
        Me.TxbBirthDate.ValidatingType = GetType(Date)
        '
        'LblBirthDate
        '
        Me.LblBirthDate.AutoSize = True
        Me.LblBirthDate.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.LblBirthDate.Location = New System.Drawing.Point(27, 275)
        Me.LblBirthDate.Name = "LblBirthDate"
        Me.LblBirthDate.Size = New System.Drawing.Size(102, 13)
        Me.LblBirthDate.TabIndex = 29
        Me.LblBirthDate.Text = "Date de naissance :"
        '
        'DtpBirthDate
        '
        Me.DtpBirthDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DtpBirthDate.Location = New System.Drawing.Point(28, 291)
        Me.DtpBirthDate.Name = "DtpBirthDate"
        Me.DtpBirthDate.Size = New System.Drawing.Size(122, 20)
        Me.DtpBirthDate.TabIndex = 30
        '
        'CbxCategory
        '
        Me.CbxCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbxCategory.FormattingEnabled = True
        Me.CbxCategory.Location = New System.Drawing.Point(29, 238)
        Me.CbxCategory.Name = "CbxCategory"
        Me.CbxCategory.Size = New System.Drawing.Size(121, 21)
        Me.CbxCategory.Sorted = True
        Me.CbxCategory.TabIndex = 28
        '
        'LblCategory
        '
        Me.LblCategory.AutoSize = True
        Me.LblCategory.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.LblCategory.Location = New System.Drawing.Point(26, 222)
        Me.LblCategory.Name = "LblCategory"
        Me.LblCategory.Size = New System.Drawing.Size(58, 13)
        Me.LblCategory.TabIndex = 27
        Me.LblCategory.Text = "Catégorie :"
        '
        'CbxSex
        '
        Me.CbxSex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbxSex.FormattingEnabled = True
        Me.CbxSex.Items.AddRange(New Object() {"Femme", "Homme"})
        Me.CbxSex.Location = New System.Drawing.Point(28, 188)
        Me.CbxSex.Name = "CbxSex"
        Me.CbxSex.Size = New System.Drawing.Size(121, 21)
        Me.CbxSex.Sorted = True
        Me.CbxSex.TabIndex = 26
        '
        'LblSex
        '
        Me.LblSex.AutoSize = True
        Me.LblSex.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.LblSex.Location = New System.Drawing.Point(25, 172)
        Me.LblSex.Name = "LblSex"
        Me.LblSex.Size = New System.Drawing.Size(37, 13)
        Me.LblSex.TabIndex = 25
        Me.LblSex.Text = "Sexe :"
        '
        'TxbFirstName
        '
        Me.TxbFirstName.Location = New System.Drawing.Point(28, 138)
        Me.TxbFirstName.Name = "TxbFirstName"
        Me.TxbFirstName.Size = New System.Drawing.Size(195, 20)
        Me.TxbFirstName.TabIndex = 24
        Me.TxbFirstName.UseRegularExpression = False
        Me.TxbFirstName.UseRegularExpressionErrorMessage = Nothing
        Me.TxbFirstName.UseRegularExpressionPattern = Nothing
        '
        'LblFirstName
        '
        Me.LblFirstName.AutoSize = True
        Me.LblFirstName.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.LblFirstName.Location = New System.Drawing.Point(25, 122)
        Me.LblFirstName.Name = "LblFirstName"
        Me.LblFirstName.Size = New System.Drawing.Size(49, 13)
        Me.LblFirstName.TabIndex = 23
        Me.LblFirstName.Text = "Prénom :"
        '
        'TxbName
        '
        Me.TxbName.Location = New System.Drawing.Point(28, 88)
        Me.TxbName.Name = "TxbName"
        Me.TxbName.Size = New System.Drawing.Size(195, 20)
        Me.TxbName.TabIndex = 21
        Me.TxbName.UseRegularExpression = False
        Me.TxbName.UseRegularExpressionErrorMessage = Nothing
        Me.TxbName.UseRegularExpressionPattern = Nothing
        '
        'TxbBibNumber
        '
        Me.TxbBibNumber.Location = New System.Drawing.Point(28, 40)
        Me.TxbBibNumber.Name = "TxbBibNumber"
        Me.TxbBibNumber.ReadOnly = True
        Me.TxbBibNumber.Size = New System.Drawing.Size(77, 20)
        Me.TxbBibNumber.TabIndex = 22
        Me.TxbBibNumber.UseRegularExpression = False
        Me.TxbBibNumber.UseRegularExpressionErrorMessage = Nothing
        Me.TxbBibNumber.UseRegularExpressionPattern = Nothing
        '
        'LblName
        '
        Me.LblName.AutoSize = True
        Me.LblName.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.LblName.Location = New System.Drawing.Point(25, 72)
        Me.LblName.Name = "LblName"
        Me.LblName.Size = New System.Drawing.Size(38, 13)
        Me.LblName.TabIndex = 20
        Me.LblName.Text = "NOM :"
        '
        'LblBibNumber
        '
        Me.LblBibNumber.AutoSize = True
        Me.LblBibNumber.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.LblBibNumber.Location = New System.Drawing.Point(25, 24)
        Me.LblBibNumber.Name = "LblBibNumber"
        Me.LblBibNumber.Size = New System.Drawing.Size(80, 13)
        Me.LblBibNumber.TabIndex = 19
        Me.LblBibNumber.Text = "N° de dossard :"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.BtnValidate, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.BtnCancel, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(125, 9)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'BtnValidate
        '
        Me.BtnValidate.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnValidate.Enabled = False
        Me.BtnValidate.Location = New System.Drawing.Point(3, 3)
        Me.BtnValidate.Name = "BtnValidate"
        Me.BtnValidate.Size = New System.Drawing.Size(67, 23)
        Me.BtnValidate.TabIndex = 0
        Me.BtnValidate.Text = "Ajouter"
        '
        'BtnCancel
        '
        Me.BtnCancel.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BtnCancel.Location = New System.Drawing.Point(76, 3)
        Me.BtnCancel.Name = "BtnCancel"
        Me.BtnCancel.Size = New System.Drawing.Size(67, 23)
        Me.BtnCancel.TabIndex = 1
        Me.BtnCancel.Text = "Annuler"
        '
        'DlgMember
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(274, 507)
        Me.Controls.Add(Me.SplitContainer1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "DlgMember"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Ajouter un participant"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents BtnValidate As System.Windows.Forms.Button
    Friend WithEvents BtnCancel As System.Windows.Forms.Button
    Friend WithEvents TxbClub As Competition.Extensions.TextBoxEx
    Friend WithEvents LblPlace As System.Windows.Forms.Label
    Friend WithEvents TxbBirthDate As System.Windows.Forms.MaskedTextBox
    Friend WithEvents LblBirthDate As System.Windows.Forms.Label
    Friend WithEvents DtpBirthDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents CbxCategory As System.Windows.Forms.ComboBox
    Friend WithEvents LblCategory As System.Windows.Forms.Label
    Friend WithEvents CbxSex As System.Windows.Forms.ComboBox
    Friend WithEvents LblSex As System.Windows.Forms.Label
    Friend WithEvents TxbFirstName As Competition.Extensions.TextBoxEx
    Friend WithEvents LblFirstName As System.Windows.Forms.Label
    Friend WithEvents TxbName As Competition.Extensions.TextBoxEx
    Friend WithEvents TxbBibNumber As Competition.Extensions.TextBoxEx
    Friend WithEvents LblName As System.Windows.Forms.Label
    Friend WithEvents LblBibNumber As System.Windows.Forms.Label
    Friend WithEvents TxbComment As System.Windows.Forms.TextBox
    Friend WithEvents LblComment As System.Windows.Forms.Label

End Class
