﻿Imports System.Windows.Forms
Imports Competition.Settings
Imports Competition.Core

Public Class DlgRace

#Region " Fields "
    Private m_Param As AppSettings
    Private m_Modify As Boolean
#End Region

#Region " Properties "
    Public Property NewRace As Race
    Public Property CurrentRace As Race
#End Region

#Region " Constructor "
    Sub New(Param As AppSettings)
        InitializeComponent()
        Me.ActiveControl = TxbName
        m_Param = Param
        NewRace = New Race
        NewRace.Frm = Param.Frm
    End Sub

    Sub New(Param As AppSettings, CurrRace As Race)
        InitializeComponent()
        Me.ActiveControl = TxbName
        m_Modify = True
        m_Param = Param
        NewRace = New Race
        NewRace.Frm = Param.Frm
        CurrentRace = CurrRace
        Me.Text = "Modifier une course"
        Me.BtnCreate.Text = "Modifier"
        With CurrRace
            TxbName.Text = .Name
            TxbDate.Text = .Date
            TxbDistance.Text = .Distance
            TxbPlace.Text = .Place
            TxbComment.Text = .Comment
            If Not .Categories.Count = 0 Then
                For Each cat In .Categories
                    Dim str As String() = New String(0) {cat.Value}
                    Dim lvi As New ListViewItem(str)
                    lvi.Tag = cat.Value
                    LvCategories.Items.Add(lvi)
                Next
            End If
        End With
    End Sub
#End Region

#Region " Methods "
    Private Sub BtnCreate_Click(sender As System.Object, e As System.EventArgs) Handles BtnCreate.Click

        Dim alreadyExists = If(m_Modify, m_Param.Races.Any(Function(f) f.Name.ToLower = TxbName.Text.ToLower AndAlso Not f.Name.ToLower = CurrentRace.Name.ToLower), _
                            m_Param.Races.Any(Function(f) f.Name.ToLower = TxbName.Text.ToLower))

        If alreadyExists Then
            MessageBox.Show("Ce nom existe déjà, veuillez en saisir un autre !", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        Else
            Me.DialogResult = System.Windows.Forms.DialogResult.OK
            With NewRace
                .Name = TxbName.Text
                .Date = TxbDate.Text
                .Distance = TxbDistance.Text
                .Place = TxbPlace.Text
                .Comment = TxbComment.Text

                .Categories.Clear()
                For Each it As ListViewItem In LvCategories.Items
                    .Categories.Add(New Category() With {.Value = it.Text})
                Next

                If Not CurrentRace Is Nothing Then
                    For Each Member In CurrentRace.Members
                        Dim Exists = LvCategories.Items.Cast(Of ListViewItem).Any(Function(f) f.Text = Member.Category)
                        If Exists = False Then Member.Category = String.Empty
                        .Members.Add(Member)
                    Next
                    m_Param.Races.Remove(CurrentRace)
                End If
            End With
            m_Param.Races.Add(NewRace)
            m_Param.SaveFile()
            Me.Close()
        End If
    End Sub

    Private Sub LvCategories_ItemSelectionChanged(sender As Object, e As ListViewItemSelectionChangedEventArgs) Handles LvCategories.ItemSelectionChanged
        If e.IsSelected Then
            TsBtnRemoveCategory.Enabled = True
        Else
            TsBtnRemoveCategory.Enabled = False
        End If
    End Sub

    Private Sub BtnCancel_Click(sender As System.Object, e As System.EventArgs) Handles BtnCancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub TxbName_TextChanged(sender As Object, e As EventArgs) Handles TxbName.TextChanged
        Dim Enabled = TxbName.Text.Length > 0
        BtnCreate.Enabled = Enabled
        GbxNewCategory.Enabled = Enabled
    End Sub

    Private Sub DtpDate_ValueChanged(sender As Object, e As EventArgs) Handles DtpDate.ValueChanged
        TxbDate.Text = String.Format("{0:MM/dd/yyyy}", DtpDate.Value)
    End Sub

    Private Sub TsBtnAddCategory_Click(sender As Object, e As EventArgs) Handles TsBtnAddCategory.Click
        Dim Exists = LvCategories.Items.Cast(Of ListViewItem).Any(Function(f) f.Text.ToLower = TsTxbCategoryName.Text.ToLower)
        If Not Exists Then
            If Not String.IsNullOrEmpty(TsTxbCategoryName.Text) Then
                Dim str As String() = New String(0) {TsTxbCategoryName.Text}
                Dim lvi As New ListViewItem(str)
                lvi.Tag = TsTxbCategoryName.Text
                LvCategories.Items.Add(lvi)
                TsTxbCategoryName.Text = String.Empty
                TsTxbCategoryName.Focus()
            End If
        End If
    End Sub

    Private Sub TsBtnRemoveCategory_Click(sender As Object, e As EventArgs) Handles TsBtnRemoveCategory.Click
        For Each it As ListViewItem In LvCategories.SelectedItems
            it.Remove()
        Next
    End Sub
#End Region

End Class
