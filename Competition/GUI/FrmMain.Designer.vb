﻿Imports Competition.Extensions

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMain
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim CheckBoxProperties1 As Competition.Extensions.CheckBoxProperties = New Competition.Extensions.CheckBoxProperties()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMain))
        Me.SpcMain = New System.Windows.Forms.SplitContainer()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.GbxRaces = New System.Windows.Forms.GroupBox()
        Me.LvRaces = New Competition.Extensions.ListViewEx()
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TsRaces = New System.Windows.Forms.ToolStrip()
        Me.TsBtnRaceAdd = New System.Windows.Forms.ToolStripButton()
        Me.TsBtnRaceRemove = New System.Windows.Forms.ToolStripButton()
        Me.TsBtnRaceModify = New System.Windows.Forms.ToolStripButton()
        Me.GbxMembers = New System.Windows.Forms.GroupBox()
        Me.LvMembers = New Competition.Extensions.ListViewEx()
        Me.ColumnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader12 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader13 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader14 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader15 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader16 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader17 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader22 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TsMembers = New System.Windows.Forms.ToolStrip()
        Me.TsBtnMemberAdd = New System.Windows.Forms.ToolStripButton()
        Me.TsBtnMemberRemove = New System.Windows.Forms.ToolStripButton()
        Me.TsBtnMemberModify = New System.Windows.Forms.ToolStripButton()
        Me.SplitContainer3 = New System.Windows.Forms.SplitContainer()
        Me.LblTitle = New System.Windows.Forms.Label()
        Me.PnlMain = New System.Windows.Forms.Panel()
        Me.TxbEditor = New Competition.Extensions.TextBoxEx()
        Me.GbxArrived = New System.Windows.Forms.GroupBox()
        Me.CbxArrivedHideColumns = New Competition.Extensions.CheckBoxComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.TsDdBtnPrint = New System.Windows.Forms.ToolStripDropDownButton()
        Me.TsMiBtnSetup = New System.Windows.Forms.ToolStripMenuItem()
        Me.TsMiBtnPreview = New System.Windows.Forms.ToolStripMenuItem()
        Me.TsMiBtnPrint = New System.Windows.Forms.ToolStripMenuItem()
        Me.LvArrived = New Competition.Extensions.ListViewEx()
        Me.ClhArrivedPlace = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ClhArrivedChrono = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ClhArrivedDossard = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ClhArrivedNom = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ClhArrivedPrenom = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ClhArrivedSexe = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ClhArrivedCategorie = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ClhArrivedDateNaissance = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ClhArrivedClub = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.BtnNewArriving = New System.Windows.Forms.Button()
        Me.LblElapsed = New System.Windows.Forms.Label()
        Me.BtnStartStop = New System.Windows.Forms.Button()
        Me.SpcMain.Panel1.SuspendLayout()
        Me.SpcMain.Panel2.SuspendLayout()
        Me.SpcMain.SuspendLayout()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.GbxRaces.SuspendLayout()
        Me.TsRaces.SuspendLayout()
        Me.GbxMembers.SuspendLayout()
        Me.TsMembers.SuspendLayout()
        Me.SplitContainer3.Panel1.SuspendLayout()
        Me.SplitContainer3.Panel2.SuspendLayout()
        Me.SplitContainer3.SuspendLayout()
        Me.PnlMain.SuspendLayout()
        Me.GbxArrived.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpcMain
        '
        Me.SpcMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SpcMain.IsSplitterFixed = True
        Me.SpcMain.Location = New System.Drawing.Point(0, 0)
        Me.SpcMain.Name = "SpcMain"
        '
        'SpcMain.Panel1
        '
        Me.SpcMain.Panel1.Controls.Add(Me.SplitContainer2)
        '
        'SpcMain.Panel2
        '
        Me.SpcMain.Panel2.Controls.Add(Me.SplitContainer3)
        Me.SpcMain.Size = New System.Drawing.Size(1305, 761)
        Me.SpcMain.SplitterDistance = 400
        Me.SpcMain.TabIndex = 4
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer2.Name = "SplitContainer2"
        Me.SplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.GbxRaces)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.GbxMembers)
        Me.SplitContainer2.Size = New System.Drawing.Size(400, 761)
        Me.SplitContainer2.SplitterDistance = 175
        Me.SplitContainer2.TabIndex = 17
        '
        'GbxRaces
        '
        Me.GbxRaces.Controls.Add(Me.LvRaces)
        Me.GbxRaces.Controls.Add(Me.TsRaces)
        Me.GbxRaces.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GbxRaces.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.GbxRaces.Location = New System.Drawing.Point(0, 0)
        Me.GbxRaces.Name = "GbxRaces"
        Me.GbxRaces.Size = New System.Drawing.Size(400, 175)
        Me.GbxRaces.TabIndex = 1
        Me.GbxRaces.TabStop = False
        Me.GbxRaces.Text = "Courses (0)"
        '
        'LvRaces
        '
        Me.LvRaces.AllowColumnReorder = True
        Me.LvRaces.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader6, Me.ColumnHeader7, Me.ColumnHeader8, Me.ColumnHeader9, Me.ColumnHeader10})
        Me.LvRaces.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LvRaces.DoubleClickActivation = False
        Me.LvRaces.FitToPage = False
        Me.LvRaces.FullRowSelect = True
        Me.LvRaces.Location = New System.Drawing.Point(3, 41)
        Me.LvRaces.MultiSelect = False
        Me.LvRaces.Name = "LvRaces"
        Me.LvRaces.ShowGroups = False
        Me.LvRaces.ShowItemToolTips = True
        Me.LvRaces.Size = New System.Drawing.Size(394, 131)
        Me.LvRaces.Sorting = System.Windows.Forms.SortOrder.Descending
        Me.LvRaces.TabIndex = 3
        Me.LvRaces.Title = ""
        Me.LvRaces.UseCompatibleStateImageBehavior = False
        Me.LvRaces.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Nom"
        Me.ColumnHeader6.Width = 133
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Date"
        Me.ColumnHeader7.Width = 79
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Distance"
        Me.ColumnHeader8.Width = 54
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Lieu"
        Me.ColumnHeader9.Width = 125
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "Commentaire"
        Me.ColumnHeader10.Width = 155
        '
        'TsRaces
        '
        Me.TsRaces.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TsRaces.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TsBtnRaceAdd, Me.TsBtnRaceRemove, Me.TsBtnRaceModify})
        Me.TsRaces.Location = New System.Drawing.Point(3, 16)
        Me.TsRaces.Name = "TsRaces"
        Me.TsRaces.Size = New System.Drawing.Size(394, 25)
        Me.TsRaces.TabIndex = 2
        Me.TsRaces.Text = "ToolStrip1"
        '
        'TsBtnRaceAdd
        '
        Me.TsBtnRaceAdd.Image = Global.Competition.My.Resources.Resources.Add
        Me.TsBtnRaceAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnRaceAdd.Name = "TsBtnRaceAdd"
        Me.TsBtnRaceAdd.Size = New System.Drawing.Size(55, 22)
        Me.TsBtnRaceAdd.Text = "Créer"
        '
        'TsBtnRaceRemove
        '
        Me.TsBtnRaceRemove.Enabled = False
        Me.TsBtnRaceRemove.Image = Global.Competition.My.Resources.Resources.Remove
        Me.TsBtnRaceRemove.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnRaceRemove.Name = "TsBtnRaceRemove"
        Me.TsBtnRaceRemove.Size = New System.Drawing.Size(82, 22)
        Me.TsBtnRaceRemove.Text = "Supprimer"
        '
        'TsBtnRaceModify
        '
        Me.TsBtnRaceModify.Enabled = False
        Me.TsBtnRaceModify.Image = Global.Competition.My.Resources.Resources.Edit
        Me.TsBtnRaceModify.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnRaceModify.Name = "TsBtnRaceModify"
        Me.TsBtnRaceModify.Size = New System.Drawing.Size(72, 22)
        Me.TsBtnRaceModify.Text = "Modifier"
        '
        'GbxMembers
        '
        Me.GbxMembers.Controls.Add(Me.LvMembers)
        Me.GbxMembers.Controls.Add(Me.TsMembers)
        Me.GbxMembers.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GbxMembers.Enabled = False
        Me.GbxMembers.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.GbxMembers.Location = New System.Drawing.Point(0, 0)
        Me.GbxMembers.Name = "GbxMembers"
        Me.GbxMembers.Size = New System.Drawing.Size(400, 582)
        Me.GbxMembers.TabIndex = 1
        Me.GbxMembers.TabStop = False
        Me.GbxMembers.Text = "Participants (0)"
        '
        'LvMembers
        '
        Me.LvMembers.AllowColumnReorder = True
        Me.LvMembers.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader11, Me.ColumnHeader12, Me.ColumnHeader13, Me.ColumnHeader14, Me.ColumnHeader15, Me.ColumnHeader16, Me.ColumnHeader17, Me.ColumnHeader22})
        Me.LvMembers.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LvMembers.DoubleClickActivation = False
        Me.LvMembers.FitToPage = False
        Me.LvMembers.FullRowSelect = True
        Me.LvMembers.Location = New System.Drawing.Point(3, 41)
        Me.LvMembers.Name = "LvMembers"
        Me.LvMembers.ShowGroups = False
        Me.LvMembers.Size = New System.Drawing.Size(394, 538)
        Me.LvMembers.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.LvMembers.TabIndex = 4
        Me.LvMembers.Title = ""
        Me.LvMembers.UseCompatibleStateImageBehavior = False
        Me.LvMembers.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "Dossard"
        Me.ColumnHeader11.Width = 51
        '
        'ColumnHeader12
        '
        Me.ColumnHeader12.Text = "Nom"
        Me.ColumnHeader12.Width = 104
        '
        'ColumnHeader13
        '
        Me.ColumnHeader13.Text = "Prénom"
        Me.ColumnHeader13.Width = 112
        '
        'ColumnHeader14
        '
        Me.ColumnHeader14.Text = "Sexe"
        Me.ColumnHeader14.Width = 54
        '
        'ColumnHeader15
        '
        Me.ColumnHeader15.Text = "Catégorie"
        Me.ColumnHeader15.Width = 68
        '
        'ColumnHeader16
        '
        Me.ColumnHeader16.Text = "Date de naissance"
        Me.ColumnHeader16.Width = 109
        '
        'ColumnHeader17
        '
        Me.ColumnHeader17.Text = "Club"
        Me.ColumnHeader17.Width = 80
        '
        'ColumnHeader22
        '
        Me.ColumnHeader22.Text = "Commentaire"
        '
        'TsMembers
        '
        Me.TsMembers.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TsMembers.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TsBtnMemberAdd, Me.TsBtnMemberRemove, Me.TsBtnMemberModify})
        Me.TsMembers.Location = New System.Drawing.Point(3, 16)
        Me.TsMembers.Name = "TsMembers"
        Me.TsMembers.Size = New System.Drawing.Size(394, 25)
        Me.TsMembers.TabIndex = 3
        Me.TsMembers.Text = "ToolStrip2"
        '
        'TsBtnMemberAdd
        '
        Me.TsBtnMemberAdd.Image = Global.Competition.My.Resources.Resources.Add
        Me.TsBtnMemberAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnMemberAdd.Name = "TsBtnMemberAdd"
        Me.TsBtnMemberAdd.Size = New System.Drawing.Size(66, 22)
        Me.TsBtnMemberAdd.Text = "Ajouter"
        '
        'TsBtnMemberRemove
        '
        Me.TsBtnMemberRemove.Enabled = False
        Me.TsBtnMemberRemove.Image = Global.Competition.My.Resources.Resources.Remove
        Me.TsBtnMemberRemove.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnMemberRemove.Name = "TsBtnMemberRemove"
        Me.TsBtnMemberRemove.Size = New System.Drawing.Size(82, 22)
        Me.TsBtnMemberRemove.Text = "Supprimer"
        '
        'TsBtnMemberModify
        '
        Me.TsBtnMemberModify.Enabled = False
        Me.TsBtnMemberModify.Image = Global.Competition.My.Resources.Resources.Edit
        Me.TsBtnMemberModify.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnMemberModify.Name = "TsBtnMemberModify"
        Me.TsBtnMemberModify.Size = New System.Drawing.Size(72, 22)
        Me.TsBtnMemberModify.Text = "Modifier"
        '
        'SplitContainer3
        '
        Me.SplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer3.IsSplitterFixed = True
        Me.SplitContainer3.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer3.Name = "SplitContainer3"
        Me.SplitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer3.Panel1
        '
        Me.SplitContainer3.Panel1.Controls.Add(Me.LblTitle)
        '
        'SplitContainer3.Panel2
        '
        Me.SplitContainer3.Panel2.Controls.Add(Me.PnlMain)
        Me.SplitContainer3.Size = New System.Drawing.Size(901, 761)
        Me.SplitContainer3.SplitterDistance = 43
        Me.SplitContainer3.TabIndex = 0
        '
        'LblTitle
        '
        Me.LblTitle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTitle.ForeColor = System.Drawing.SystemColors.Highlight
        Me.LblTitle.Location = New System.Drawing.Point(0, 0)
        Me.LblTitle.Name = "LblTitle"
        Me.LblTitle.Size = New System.Drawing.Size(901, 43)
        Me.LblTitle.TabIndex = 19
        Me.LblTitle.Text = "Veuillez charger ou créer une nouvelle course"
        Me.LblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PnlMain
        '
        Me.PnlMain.Controls.Add(Me.TxbEditor)
        Me.PnlMain.Controls.Add(Me.GbxArrived)
        Me.PnlMain.Controls.Add(Me.BtnNewArriving)
        Me.PnlMain.Controls.Add(Me.LblElapsed)
        Me.PnlMain.Controls.Add(Me.BtnStartStop)
        Me.PnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlMain.Enabled = False
        Me.PnlMain.Location = New System.Drawing.Point(0, 0)
        Me.PnlMain.Name = "PnlMain"
        Me.PnlMain.Size = New System.Drawing.Size(901, 714)
        Me.PnlMain.TabIndex = 17
        '
        'TxbEditor
        '
        Me.TxbEditor.Location = New System.Drawing.Point(4, 3)
        Me.TxbEditor.Name = "TxbEditor"
        Me.TxbEditor.Size = New System.Drawing.Size(91, 20)
        Me.TxbEditor.TabIndex = 1
        Me.TxbEditor.UseRegularExpression = True
        Me.TxbEditor.UseRegularExpressionErrorMessage = Nothing
        Me.TxbEditor.UseRegularExpressionPattern = "^[1-9][0-9]*$"
        Me.TxbEditor.Visible = False
        '
        'GbxArrived
        '
        Me.GbxArrived.Controls.Add(Me.CbxArrivedHideColumns)
        Me.GbxArrived.Controls.Add(Me.Label1)
        Me.GbxArrived.Controls.Add(Me.ToolStrip1)
        Me.GbxArrived.Controls.Add(Me.LvArrived)
        Me.GbxArrived.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.GbxArrived.Location = New System.Drawing.Point(4, 68)
        Me.GbxArrived.Name = "GbxArrived"
        Me.GbxArrived.Size = New System.Drawing.Size(889, 643)
        Me.GbxArrived.TabIndex = 12
        Me.GbxArrived.TabStop = False
        Me.GbxArrived.Text = "Pointage des arrivées"
        '
        'CbxArrivedHideColumns
        '
        CheckBoxProperties1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CbxArrivedHideColumns.CheckBoxProperties = CheckBoxProperties1
        Me.CbxArrivedHideColumns.DisplayMemberSingleItem = ""
        Me.CbxArrivedHideColumns.FormattingEnabled = True
        Me.CbxArrivedHideColumns.Items.AddRange(New Object() {"Place", "Chrono", "Dossard", "Nom", "Prénom", "Sexe", "Catégorie", "Date de naissance", "Club"})
        Me.CbxArrivedHideColumns.Location = New System.Drawing.Point(171, 19)
        Me.CbxArrivedHideColumns.Name = "CbxArrivedHideColumns"
        Me.CbxArrivedHideColumns.Size = New System.Drawing.Size(455, 21)
        Me.CbxArrivedHideColumns.TabIndex = 24
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(160, 13)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "Afficher/masquer des colonnes :"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TsDdBtnPrint})
        Me.ToolStrip1.Location = New System.Drawing.Point(790, 19)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(97, 25)
        Me.ToolStrip1.TabIndex = 22
        '
        'TsDdBtnPrint
        '
        Me.TsDdBtnPrint.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TsDdBtnPrint.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TsMiBtnSetup, Me.TsMiBtnPreview, Me.TsMiBtnPrint})
        Me.TsDdBtnPrint.Image = Global.Competition.My.Resources.Resources.Print
        Me.TsDdBtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsDdBtnPrint.Name = "TsDdBtnPrint"
        Me.TsDdBtnPrint.Size = New System.Drawing.Size(94, 22)
        Me.TsDdBtnPrint.Text = "Impression"
        '
        'TsMiBtnSetup
        '
        Me.TsMiBtnSetup.Name = "TsMiBtnSetup"
        Me.TsMiBtnSetup.Size = New System.Drawing.Size(133, 22)
        Me.TsMiBtnSetup.Text = "Paramètres"
        '
        'TsMiBtnPreview
        '
        Me.TsMiBtnPreview.Name = "TsMiBtnPreview"
        Me.TsMiBtnPreview.Size = New System.Drawing.Size(133, 22)
        Me.TsMiBtnPreview.Text = "Aperçu"
        '
        'TsMiBtnPrint
        '
        Me.TsMiBtnPrint.Name = "TsMiBtnPrint"
        Me.TsMiBtnPrint.Size = New System.Drawing.Size(133, 22)
        Me.TsMiBtnPrint.Text = "Imprimer"
        '
        'LvArrived
        '
        Me.LvArrived.AllowColumnReorder = True
        Me.LvArrived.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ClhArrivedPlace, Me.ClhArrivedChrono, Me.ClhArrivedDossard, Me.ClhArrivedNom, Me.ClhArrivedPrenom, Me.ClhArrivedSexe, Me.ClhArrivedCategorie, Me.ClhArrivedDateNaissance, Me.ClhArrivedClub})
        Me.LvArrived.DoubleClickActivation = False
        Me.LvArrived.FitToPage = False
        Me.LvArrived.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LvArrived.FullRowSelect = True
        Me.LvArrived.Location = New System.Drawing.Point(0, 46)
        Me.LvArrived.MultiSelect = False
        Me.LvArrived.Name = "LvArrived"
        Me.LvArrived.ShowGroups = False
        Me.LvArrived.Size = New System.Drawing.Size(883, 597)
        Me.LvArrived.Sorting = System.Windows.Forms.SortOrder.Descending
        Me.LvArrived.TabIndex = 0
        Me.LvArrived.Title = ""
        Me.LvArrived.UseCompatibleStateImageBehavior = False
        Me.LvArrived.View = System.Windows.Forms.View.Details
        '
        'ClhArrivedPlace
        '
        Me.ClhArrivedPlace.Text = "Place"
        Me.ClhArrivedPlace.Width = 47
        '
        'ClhArrivedChrono
        '
        Me.ClhArrivedChrono.Text = "Chrono"
        Me.ClhArrivedChrono.Width = 80
        '
        'ClhArrivedDossard
        '
        Me.ClhArrivedDossard.Text = "Dossard"
        Me.ClhArrivedDossard.Width = 64
        '
        'ClhArrivedNom
        '
        Me.ClhArrivedNom.Text = "Nom"
        Me.ClhArrivedNom.Width = 132
        '
        'ClhArrivedPrenom
        '
        Me.ClhArrivedPrenom.Text = "Prénom"
        Me.ClhArrivedPrenom.Width = 120
        '
        'ClhArrivedSexe
        '
        Me.ClhArrivedSexe.Text = "Sexe"
        Me.ClhArrivedSexe.Width = 63
        '
        'ClhArrivedCategorie
        '
        Me.ClhArrivedCategorie.Text = "Categorie"
        Me.ClhArrivedCategorie.Width = 89
        '
        'ClhArrivedDateNaissance
        '
        Me.ClhArrivedDateNaissance.Text = "Date de naissance"
        Me.ClhArrivedDateNaissance.Width = 125
        '
        'ClhArrivedClub
        '
        Me.ClhArrivedClub.Text = "Club"
        Me.ClhArrivedClub.Width = 158
        '
        'BtnNewArriving
        '
        Me.BtnNewArriving.Enabled = False
        Me.BtnNewArriving.Image = Global.Competition.My.Resources.Resources.Arrived
        Me.BtnNewArriving.Location = New System.Drawing.Point(625, 2)
        Me.BtnNewArriving.Name = "BtnNewArriving"
        Me.BtnNewArriving.Size = New System.Drawing.Size(60, 60)
        Me.BtnNewArriving.TabIndex = 11
        Me.BtnNewArriving.UseVisualStyleBackColor = True
        '
        'LblElapsed
        '
        Me.LblElapsed.BackColor = System.Drawing.Color.White
        Me.LblElapsed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LblElapsed.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblElapsed.Location = New System.Drawing.Point(354, 12)
        Me.LblElapsed.Name = "LblElapsed"
        Me.LblElapsed.Size = New System.Drawing.Size(191, 43)
        Me.LblElapsed.TabIndex = 10
        Me.LblElapsed.Text = "00:00:00.00"
        Me.LblElapsed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'BtnStartStop
        '
        Me.BtnStartStop.Image = Global.Competition.My.Resources.Resources.Play
        Me.BtnStartStop.Location = New System.Drawing.Point(216, 2)
        Me.BtnStartStop.Name = "BtnStartStop"
        Me.BtnStartStop.Size = New System.Drawing.Size(60, 60)
        Me.BtnStartStop.TabIndex = 8
        Me.BtnStartStop.Tag = "Démarrer"
        Me.BtnStartStop.UseVisualStyleBackColor = True
        '
        'FrmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1305, 761)
        Me.Controls.Add(Me.SpcMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "FrmMain"
        Me.Text = "Compétition"
        Me.SpcMain.Panel1.ResumeLayout(False)
        Me.SpcMain.Panel2.ResumeLayout(False)
        Me.SpcMain.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        Me.SplitContainer2.ResumeLayout(False)
        Me.GbxRaces.ResumeLayout(False)
        Me.GbxRaces.PerformLayout()
        Me.TsRaces.ResumeLayout(False)
        Me.TsRaces.PerformLayout()
        Me.GbxMembers.ResumeLayout(False)
        Me.GbxMembers.PerformLayout()
        Me.TsMembers.ResumeLayout(False)
        Me.TsMembers.PerformLayout()
        Me.SplitContainer3.Panel1.ResumeLayout(False)
        Me.SplitContainer3.Panel2.ResumeLayout(False)
        Me.SplitContainer3.ResumeLayout(False)
        Me.PnlMain.ResumeLayout(False)
        Me.PnlMain.PerformLayout()
        Me.GbxArrived.ResumeLayout(False)
        Me.GbxArrived.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents SpcMain As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer3 As System.Windows.Forms.SplitContainer
    Friend WithEvents LblTitle As System.Windows.Forms.Label
    Friend WithEvents PnlMain As System.Windows.Forms.Panel
    Friend WithEvents GbxArrived As System.Windows.Forms.GroupBox
    Friend WithEvents LvArrived As ListViewEx
    Friend WithEvents ClhArrivedPlace As System.Windows.Forms.ColumnHeader
    Friend WithEvents ClhArrivedChrono As System.Windows.Forms.ColumnHeader
    Friend WithEvents ClhArrivedDossard As System.Windows.Forms.ColumnHeader
    Friend WithEvents ClhArrivedNom As System.Windows.Forms.ColumnHeader
    Friend WithEvents ClhArrivedPrenom As System.Windows.Forms.ColumnHeader
    Friend WithEvents BtnNewArriving As System.Windows.Forms.Button
    Friend WithEvents LblElapsed As System.Windows.Forms.Label
    Friend WithEvents BtnStartStop As System.Windows.Forms.Button
    Friend WithEvents GbxRaces As System.Windows.Forms.GroupBox
    Friend WithEvents TsRaces As System.Windows.Forms.ToolStrip
    Friend WithEvents TsBtnRaceAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents TsBtnRaceRemove As System.Windows.Forms.ToolStripButton
    Friend WithEvents TsBtnRaceModify As System.Windows.Forms.ToolStripButton
    Friend WithEvents GbxMembers As System.Windows.Forms.GroupBox
    Friend WithEvents TsMembers As System.Windows.Forms.ToolStrip
    Friend WithEvents TsBtnMemberAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents TsBtnMemberRemove As System.Windows.Forms.ToolStripButton
    Friend WithEvents TsBtnMemberModify As System.Windows.Forms.ToolStripButton
    Friend WithEvents LvRaces As ListViewEx
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader10 As System.Windows.Forms.ColumnHeader
    Friend WithEvents LvMembers As ListViewEx
    Friend WithEvents ColumnHeader11 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader12 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader13 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader14 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader15 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader16 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader17 As System.Windows.Forms.ColumnHeader
    Friend WithEvents TxbEditor As Competition.Extensions.TextBoxEx
    Friend WithEvents ClhArrivedSexe As System.Windows.Forms.ColumnHeader
    Friend WithEvents ClhArrivedCategorie As System.Windows.Forms.ColumnHeader
    Friend WithEvents ClhArrivedDateNaissance As System.Windows.Forms.ColumnHeader
    Friend WithEvents ClhArrivedClub As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader22 As System.Windows.Forms.ColumnHeader
    Friend WithEvents CbxArrivedHideColumns As Competition.Extensions.CheckBoxComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents TsDdBtnPrint As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents TsMiBtnSetup As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TsMiBtnPreview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TsMiBtnPrint As System.Windows.Forms.ToolStripMenuItem

End Class
