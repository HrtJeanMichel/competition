﻿Imports Competition.Extensions

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DlgRace
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.BtnCreate = New System.Windows.Forms.Button()
        Me.BtnCancel = New System.Windows.Forms.Button()
        Me.LblName = New System.Windows.Forms.Label()
        Me.TxbName = New System.Windows.Forms.TextBox()
        Me.LblDate = New System.Windows.Forms.Label()
        Me.DtpDate = New System.Windows.Forms.DateTimePicker()
        Me.LblDistance = New System.Windows.Forms.Label()
        Me.LblPlace = New System.Windows.Forms.Label()
        Me.TxbPlace = New System.Windows.Forms.TextBox()
        Me.TxbComment = New System.Windows.Forms.TextBox()
        Me.LblComment = New System.Windows.Forms.Label()
        Me.GbxNewRace = New System.Windows.Forms.GroupBox()
        Me.TxbDistance = New System.Windows.Forms.MaskedTextBox()
        Me.TxbDate = New System.Windows.Forms.MaskedTextBox()
        Me.GbxNewCategory = New System.Windows.Forms.GroupBox()
        Me.LvCategories = New Competition.Extensions.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.TsTxbCategoryName = New System.Windows.Forms.ToolStripTextBox()
        Me.TsBtnAddCategory = New System.Windows.Forms.ToolStripButton()
        Me.TsBtnRemoveCategory = New System.Windows.Forms.ToolStripButton()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GbxNewRace.SuspendLayout()
        Me.GbxNewCategory.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.BtnCreate, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.BtnCancel, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(198, 416)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'BtnCreate
        '
        Me.BtnCreate.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnCreate.Enabled = False
        Me.BtnCreate.Location = New System.Drawing.Point(3, 3)
        Me.BtnCreate.Name = "BtnCreate"
        Me.BtnCreate.Size = New System.Drawing.Size(67, 23)
        Me.BtnCreate.TabIndex = 6
        Me.BtnCreate.Text = "Créer"
        '
        'BtnCancel
        '
        Me.BtnCancel.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BtnCancel.Location = New System.Drawing.Point(76, 3)
        Me.BtnCancel.Name = "BtnCancel"
        Me.BtnCancel.Size = New System.Drawing.Size(67, 23)
        Me.BtnCancel.TabIndex = 7
        Me.BtnCancel.Text = "Annuler"
        '
        'LblName
        '
        Me.LblName.AutoSize = True
        Me.LblName.Location = New System.Drawing.Point(6, 19)
        Me.LblName.Name = "LblName"
        Me.LblName.Size = New System.Drawing.Size(38, 13)
        Me.LblName.TabIndex = 6
        Me.LblName.Text = "Nom : "
        '
        'TxbName
        '
        Me.TxbName.Location = New System.Drawing.Point(9, 35)
        Me.TxbName.Name = "TxbName"
        Me.TxbName.Size = New System.Drawing.Size(317, 20)
        Me.TxbName.TabIndex = 0
        '
        'LblDate
        '
        Me.LblDate.AutoSize = True
        Me.LblDate.Location = New System.Drawing.Point(8, 59)
        Me.LblDate.Name = "LblDate"
        Me.LblDate.Size = New System.Drawing.Size(36, 13)
        Me.LblDate.TabIndex = 7
        Me.LblDate.Text = "Date :"
        '
        'DtpDate
        '
        Me.DtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DtpDate.Location = New System.Drawing.Point(9, 75)
        Me.DtpDate.Name = "DtpDate"
        Me.DtpDate.Size = New System.Drawing.Size(116, 20)
        Me.DtpDate.TabIndex = 4
        '
        'LblDistance
        '
        Me.LblDistance.AutoSize = True
        Me.LblDistance.Location = New System.Drawing.Point(223, 59)
        Me.LblDistance.Name = "LblDistance"
        Me.LblDistance.Size = New System.Drawing.Size(83, 13)
        Me.LblDistance.TabIndex = 8
        Me.LblDistance.Text = "Distance (kms) :"
        '
        'LblPlace
        '
        Me.LblPlace.AutoSize = True
        Me.LblPlace.Location = New System.Drawing.Point(8, 99)
        Me.LblPlace.Name = "LblPlace"
        Me.LblPlace.Size = New System.Drawing.Size(33, 13)
        Me.LblPlace.TabIndex = 9
        Me.LblPlace.Text = "Lieu :"
        '
        'TxbPlace
        '
        Me.TxbPlace.Location = New System.Drawing.Point(9, 115)
        Me.TxbPlace.Name = "TxbPlace"
        Me.TxbPlace.Size = New System.Drawing.Size(317, 20)
        Me.TxbPlace.TabIndex = 3
        '
        'TxbComment
        '
        Me.TxbComment.Location = New System.Drawing.Point(9, 155)
        Me.TxbComment.Multiline = True
        Me.TxbComment.Name = "TxbComment"
        Me.TxbComment.Size = New System.Drawing.Size(317, 65)
        Me.TxbComment.TabIndex = 4
        '
        'LblComment
        '
        Me.LblComment.AutoSize = True
        Me.LblComment.Location = New System.Drawing.Point(8, 139)
        Me.LblComment.Name = "LblComment"
        Me.LblComment.Size = New System.Drawing.Size(74, 13)
        Me.LblComment.TabIndex = 10
        Me.LblComment.Text = "Commentaire :"
        '
        'GbxNewRace
        '
        Me.GbxNewRace.Controls.Add(Me.TxbDistance)
        Me.GbxNewRace.Controls.Add(Me.TxbDate)
        Me.GbxNewRace.Controls.Add(Me.LblName)
        Me.GbxNewRace.Controls.Add(Me.TxbName)
        Me.GbxNewRace.Controls.Add(Me.TxbComment)
        Me.GbxNewRace.Controls.Add(Me.LblDate)
        Me.GbxNewRace.Controls.Add(Me.LblComment)
        Me.GbxNewRace.Controls.Add(Me.DtpDate)
        Me.GbxNewRace.Controls.Add(Me.TxbPlace)
        Me.GbxNewRace.Controls.Add(Me.LblDistance)
        Me.GbxNewRace.Controls.Add(Me.LblPlace)
        Me.GbxNewRace.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.GbxNewRace.Location = New System.Drawing.Point(12, 12)
        Me.GbxNewRace.Name = "GbxNewRace"
        Me.GbxNewRace.Size = New System.Drawing.Size(332, 226)
        Me.GbxNewRace.TabIndex = 13
        Me.GbxNewRace.TabStop = False
        Me.GbxNewRace.Text = "Course"
        '
        'TxbDistance
        '
        Me.TxbDistance.BackColor = System.Drawing.Color.White
        Me.TxbDistance.Location = New System.Drawing.Point(226, 75)
        Me.TxbDistance.Mask = "99999"
        Me.TxbDistance.Name = "TxbDistance"
        Me.TxbDistance.RejectInputOnFirstFailure = True
        Me.TxbDistance.Size = New System.Drawing.Size(100, 20)
        Me.TxbDistance.TabIndex = 2
        Me.TxbDistance.ValidatingType = GetType(Integer)
        '
        'TxbDate
        '
        Me.TxbDate.BackColor = System.Drawing.Color.White
        Me.TxbDate.Location = New System.Drawing.Point(9, 75)
        Me.TxbDate.Mask = "00/00/0000"
        Me.TxbDate.Name = "TxbDate"
        Me.TxbDate.RejectInputOnFirstFailure = True
        Me.TxbDate.Size = New System.Drawing.Size(100, 20)
        Me.TxbDate.TabIndex = 1
        Me.TxbDate.ValidatingType = GetType(Date)
        '
        'GbxNewCategory
        '
        Me.GbxNewCategory.Controls.Add(Me.LvCategories)
        Me.GbxNewCategory.Controls.Add(Me.ToolStrip1)
        Me.GbxNewCategory.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.GbxNewCategory.Location = New System.Drawing.Point(12, 244)
        Me.GbxNewCategory.Name = "GbxNewCategory"
        Me.GbxNewCategory.Size = New System.Drawing.Size(332, 164)
        Me.GbxNewCategory.TabIndex = 15
        Me.GbxNewCategory.TabStop = False
        Me.GbxNewCategory.Text = "Catégories"
        '
        'LvCategories
        '
        Me.LvCategories.AllowColumnReorder = True
        Me.LvCategories.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.LvCategories.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LvCategories.DoubleClickActivation = False
        Me.LvCategories.FitToPage = False
        Me.LvCategories.FullRowSelect = True
        Me.LvCategories.Location = New System.Drawing.Point(3, 41)
        Me.LvCategories.Name = "LvCategories"
        Me.LvCategories.ShowGroups = False
        Me.LvCategories.Size = New System.Drawing.Size(326, 120)
        Me.LvCategories.TabIndex = 5
        Me.LvCategories.Title = ""
        Me.LvCategories.UseCompatibleStateImageBehavior = False
        Me.LvCategories.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Valeur"
        Me.ColumnHeader1.Width = 302
        '
        'ToolStrip1
        '
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TsTxbCategoryName, Me.TsBtnAddCategory, Me.TsBtnRemoveCategory})
        Me.ToolStrip1.Location = New System.Drawing.Point(3, 16)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(326, 25)
        Me.ToolStrip1.TabIndex = 2
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'TsTxbCategoryName
        '
        Me.TsTxbCategoryName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TsTxbCategoryName.Name = "TsTxbCategoryName"
        Me.TsTxbCategoryName.Size = New System.Drawing.Size(150, 25)
        '
        'TsBtnAddCategory
        '
        Me.TsBtnAddCategory.Image = Global.Competition.My.Resources.Resources.Add
        Me.TsBtnAddCategory.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnAddCategory.Name = "TsBtnAddCategory"
        Me.TsBtnAddCategory.Size = New System.Drawing.Size(66, 22)
        Me.TsBtnAddCategory.Text = "Ajouter"
        '
        'TsBtnRemoveCategory
        '
        Me.TsBtnRemoveCategory.Enabled = False
        Me.TsBtnRemoveCategory.Image = Global.Competition.My.Resources.Resources.Remove
        Me.TsBtnRemoveCategory.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnRemoveCategory.Name = "TsBtnRemoveCategory"
        Me.TsBtnRemoveCategory.Size = New System.Drawing.Size(82, 22)
        Me.TsBtnRemoveCategory.Text = "Supprimer"
        '
        'DlgRace
        '
        Me.AcceptButton = Me.BtnCreate
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.BtnCancel
        Me.ClientSize = New System.Drawing.Size(356, 453)
        Me.Controls.Add(Me.GbxNewCategory)
        Me.Controls.Add(Me.GbxNewRace)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "DlgRace"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Créer une nouvelle course"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.GbxNewRace.ResumeLayout(False)
        Me.GbxNewRace.PerformLayout()
        Me.GbxNewCategory.ResumeLayout(False)
        Me.GbxNewCategory.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents BtnCreate As System.Windows.Forms.Button
    Friend WithEvents BtnCancel As System.Windows.Forms.Button
    Friend WithEvents LblName As System.Windows.Forms.Label
    Friend WithEvents TxbName As System.Windows.Forms.TextBox
    Friend WithEvents LblDate As System.Windows.Forms.Label
    Friend WithEvents DtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblDistance As System.Windows.Forms.Label
    Friend WithEvents LblPlace As System.Windows.Forms.Label
    Friend WithEvents TxbPlace As System.Windows.Forms.TextBox
    Friend WithEvents TxbComment As System.Windows.Forms.TextBox
    Friend WithEvents LblComment As System.Windows.Forms.Label
    Friend WithEvents GbxNewRace As System.Windows.Forms.GroupBox
    Friend WithEvents TxbDate As System.Windows.Forms.MaskedTextBox
    Friend WithEvents TxbDistance As System.Windows.Forms.MaskedTextBox
    Friend WithEvents GbxNewCategory As System.Windows.Forms.GroupBox
    Friend WithEvents LvCategories As ListViewEx
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents TsTxbCategoryName As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents TsBtnAddCategory As System.Windows.Forms.ToolStripButton
    Friend WithEvents TsBtnRemoveCategory As System.Windows.Forms.ToolStripButton

End Class
