﻿Namespace Core
    Public NotInheritable Class TimerElapsedEventArgs
        Inherits EventArgs

#Region " Field "
        Private m_Elapsed As String
#End Region

#Region " Constructor "
        Public Sub New(Elapsed As String)
            m_Elapsed = Elapsed
        End Sub
#End Region

#Region " Property "
        Public ReadOnly Property Elapsed As String
            Get
                Return m_Elapsed
            End Get
        End Property
#End Region

    End Class
End Namespace
