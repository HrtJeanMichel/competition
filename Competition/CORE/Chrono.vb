﻿Imports System.Threading
Imports System.Timers

Namespace Core
    Public Class Chrono
        Implements IDisposable

#Region " Field "
        Private m_Timer As System.Windows.Forms.Timer
#End Region

#Region " Properties "
        Public Property TimeStart As DateTime
        Public Property TimeEnd As String
        Public Property TimeElapsed As String
        Public Property TimerOnGoing As Boolean
#End Region

#Region " Event "
        Public Event TimerElapsed(sender As Object, e As TimerElapsedEventArgs)
#End Region

#Region " Constructor "
        Public Sub New()
            m_Timer = New System.Windows.Forms.Timer()
            AddHandler m_Timer.Tick, AddressOf m_Timer_Tick
        End Sub
#End Region

#Region " Methods "
        Private Sub m_Timer_Tick(sender As Object, e As EventArgs)
            Try
                TimeElapsed = FormatTimeSpanHHMMSScc(DateTime.Now - TimeStart)
                RaiseEvent TimerElapsed(sender, New TimerElapsedEventArgs(TimeElapsed))
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End Sub

        Public Sub StartTimer()
            m_Timer.Start()
            TimerOnGoing = True
            TimeStart = DateTime.Now
        End Sub

        Public Sub StopTimer()
            m_Timer.Stop()
            TimerOnGoing = False
            TimeEnd = TimeElapsed
        End Sub

        Private Function FormatTimeSpanHHMMSScc(span As TimeSpan) As String
            Dim num = CInt(Strings.Mid(String.Format("{0:D3}", span.Milliseconds), 1, 2))
            Return String.Concat(New String() {span.Hours.ToString("00"), ":", span.Minutes.ToString("00"), ":", span.Seconds.ToString("00"), ".", num.ToString("00")})
        End Function
#End Region

#Region " IDisposable "
        Private disposedValue As Boolean
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    If Not m_Timer Is Nothing Then m_Timer.Dispose()
                End If
            End If
            Me.disposedValue = True
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Namespace
