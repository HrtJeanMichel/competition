﻿Imports System.Xml.Serialization
Imports Competition.Extensions

Namespace Core
    <Serializable> _
    Public Class Race

#Region " Fields "
        <XmlIgnore()> _
        Private m_Members As List(Of Member)
        <XmlIgnore()> _
        Private m_Categories As List(Of Category)
        <XmlIgnore()> _
        Private m_Ranks As List(Of Ranking)
        <XmlIgnore()> _
        Private m_Frm As FrmMain
#End Region

#Region " Properties "
        Public Property Name As String
        Public Property [Date] As String
        Public Property Distance As String
        Public Property Place As String
        Public Property Comment As String
        Public Property TotalElapsed As String = "00:00:00.00"

        Public Property Members() As List(Of Member)
            Get
                Return m_Members
            End Get
            Set(value As List(Of Member))
                m_Members = value
            End Set
        End Property

        Public Property Categories() As List(Of Category)
            Get
                Return m_Categories
            End Get
            Set(value As List(Of Category))
                m_Categories = value
            End Set
        End Property

        Public Property Ranks() As List(Of Ranking)
            Get
                Return m_Ranks
            End Get
            Set(value As List(Of Ranking))
                m_Ranks = value
            End Set
        End Property

        <XmlIgnore()> _
        Public Property Frm() As FrmMain
            Get
                Return m_Frm
            End Get
            Set(value As FrmMain)
                m_Frm = value
            End Set
        End Property
#End Region

#Region " Constructor "
        Sub New()
            m_Members = New List(Of Member)
            m_Categories = New List(Of Category)
            m_Ranks = New List(Of Ranking)
        End Sub
#End Region

#Region " Methods "

        Public Sub Load()
            m_Frm.LvMembers.Items.Clear()
            m_Frm.GbxMembers.Enabled = True
            m_Frm.TsBtnRaceRemove.Enabled = True
            m_Frm.TsBtnRaceModify.Enabled = True
            m_Frm.LblTitle.Text = Me.Name
            For Each Member In Me.Members
                Dim lvi As New ListViewItem(New String(7) {Member.Bib, Member.Name, Member.FirstName, Member.Sex, Member.Category, Member.BirthDate, Member.Club, Member.Comment})
                lvi.Tag = Member
                m_Frm.LvMembers.Items.Add(lvi)
                m_Frm.LvMembers.Rebuild()
            Next
            m_Frm.PnlMain.Enabled = Me.Members.Count > 0
            m_Frm.TsBtnMemberRemove.Enabled = False
            m_Frm.TsBtnMemberModify.Enabled = False
            m_Frm.GbxMembers.Text = "Participants (" & Me.Members.Count.ToString & ")"
            If Me.TotalElapsed <> "00:00:00.00" Then
                m_Frm.LblElapsed.Text = Me.TotalElapsed
                For Each Rank In Me.Ranks
                    Dim lvi As New ListViewItem(New String(9) {CStr(Rank.Position), Rank.Chrono, CStr(Rank.Bib), Rank.Name, Rank.FirstName, Rank.Sex, Rank.Category, Rank.BirthDate, Rank.Club, Rank.Comment})
                    If lvi.Text = 0 Then lvi.ForeColor = Color.Red
                    lvi.Tag = Rank
                    m_Frm.LvArrived.Items.Add(lvi)
                    m_Frm.LvArrived.Rebuild()
                Next
            End If
        End Sub

        Public Sub UnLoad()
            m_Frm.LblTitle.Text = "Veuillez Sélectionner une course"
            m_Frm.LvMembers.Items.Clear()
            m_Frm.GbxMembers.Enabled = False
            m_Frm.TsBtnRaceRemove.Enabled = False
            m_Frm.TsBtnRaceModify.Enabled = False
            m_Frm.LblElapsed.Text = "00:00:00.00"
            m_Frm.PnlMain.Enabled = False
            m_Frm.LvArrived.Items.Clear()
            m_Frm.GbxMembers.Text = "Participants (0)"
        End Sub

        Public Sub [Select]()
            Dim r = m_Frm.LvRaces.Items.Cast(Of ListViewItem).Where(Function(f) f.Text.ToLower = Name.ToLower).FirstOrDefault
            If Not r Is Nothing Then r.Selected = True
        End Sub

        Public Sub AddArriving()
            Try
                Dim Index = m_Frm.LvArrived.Items.Count
                Dim MaxSeq = Me.Members.Count
                If Index >= MaxSeq Then
                    m_Frm.BtnNewArriving.Enabled = False
                    Exit Sub
                End If

                Dim str As String() = New String(9) {}
                str(0) = (Index + 1).ToString
                str(1) = m_Frm.LblElapsed.Text

                Dim lvi As New ListViewItem(str)
                m_Frm.LvArrived.Items.Add(lvi)
                lvi.Selected = True
                m_Frm.LvArrived.StartEditing(m_Frm.Editors(2), lvi, 2)
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try

        End Sub

        Public Sub Start()
            If m_Frm.LblElapsed.Text <> "00:00:00.00" Then
                If MessageBox.Show("Le démarrage de ce nouveau chrono effacera les précédents résultats, voulez-vous continuer ?", "Démarrage", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Me.DeleteRanks()
                    m_Frm.Bdd.SaveFile()
                Else
                    Exit Sub
                End If
            End If
            m_Frm.BtnStartStop.Image = My.Resources._Stop
            m_Frm.Chrono.StartTimer()
            m_Frm.BtnStartStop.Tag = "Arrêter"
            m_Frm.GbxRaces.Enabled = False
            m_Frm.GbxMembers.Enabled = False
            m_Frm.BtnNewArriving.Enabled = True
            m_Frm.TsDdBtnPrint.Enabled = False
            m_Frm.Ongoing = True
        End Sub

        Public Sub [Stop]()
            m_Frm.BtnStartStop.Image = My.Resources.Play
            m_Frm.Chrono.StopTimer()
            m_Frm.BtnStartStop.Tag = "Démarrer"
            m_Frm.GbxRaces.Enabled = True
            m_Frm.GbxMembers.Enabled = True
            m_Frm.BtnNewArriving.Enabled = False
            Ranks.Clear()
            FillMissing()
            SaveResult()
            m_Frm.Bdd.SaveFile()
            m_Frm.TsDdBtnPrint.Enabled = True
            m_Frm.Ongoing = False
        End Sub

        Private Sub FillMissing()
            For Each Member In Me.Members
                Dim rnk = m_Frm.LvArrived.Items.Cast(Of ListViewItem).Any(Function(f) f.SubItems(2).Text = CStr(Member.Bib))
                If rnk = False Then
                    Dim str As String() = New String(9) {}
                    str(0) = 0
                    str(1) = "00:00:00.00"
                    With Member
                        str(2) = CStr(.Bib)
                        str(3) = .Name
                        str(4) = .FirstName
                        str(5) = .Sex
                        str(6) = .Category
                        str(7) = .BirthDate
                        str(8) = .Club
                        str(9) = .Comment
                    End With
                    Dim lvi As New ListViewItem(str)
                    lvi.ForeColor = Color.Red
                    m_Frm.LvArrived.Items.Add(lvi)
                End If
            Next
        End Sub

        Private Sub SaveResult()
            Me.TotalElapsed = m_Frm.LblElapsed.Text
            For Each it As ListViewItem In m_Frm.LvArrived.Items
                Dim rnk As New Ranking
                With rnk
                    .Position = CInt(it.Text)
                    .Chrono = it.SubItems(1).Text
                    .Bib = If(Not it.SubItems(2).Text = "", CInt(it.SubItems(2).Text), 0)
                    .Name = it.SubItems(3).Text
                    .FirstName = it.SubItems(4).Text
                    .Sex = it.SubItems(5).Text
                    .Category = it.SubItems(6).Text
                    .BirthDate = it.SubItems(7).Text
                    .Club = it.SubItems(8).Text
                    .Comment = it.SubItems(9).Text
                End With
                Me.Ranks.Add(rnk)
            Next
        End Sub

        Private Sub DeleteRanks()
            Me.Ranks.Clear()
            m_Frm.LvArrived.Items.Clear()
            m_Frm.LblElapsed.Text = "00:00:00.00"
        End Sub

        Public Sub DeleteMembers()
            For Each it As ListViewItem In m_Frm.LvMembers.SelectedItems
                Me.Members.Remove(it.Tag)
            Next
            m_Frm.Bdd.SaveFile()
            If Me.Members.Count = 0 Then
                m_Frm.TsBtnMemberRemove.Enabled = False
                m_Frm.TsBtnMemberModify.Enabled = False
            End If
            m_Frm.Bdd.LoadRaces()
        End Sub

#End Region

    End Class
End Namespace
