﻿Imports System.Drawing.Imaging

Namespace Core
    <Serializable> _
    Public Class Member

#Region " Properties "
        Public Property Bib As Integer
        Public Property Name As String
        Public Property FirstName As String
        Public Property Sex As String
        Public Property BirthDate As String
        Public Property Club As String
        Public Property Comment As String
        Public Property Photo As Bitmap
        Public Property Category As String
#End Region

    End Class
End Namespace
