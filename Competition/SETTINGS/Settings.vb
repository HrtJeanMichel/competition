﻿Imports System.Xml.Serialization
Imports System.IO
Imports Competition.Core

Namespace Settings
    <Serializable> _
    Public Class AppSettings

#Region " Fields "
        <XmlIgnore()> _
        Private m_Races As List(Of Race)
        <XmlIgnore()> _
        Private m_Frm As FrmMain
#End Region

#Region " Properties "
        Public Property Races() As List(Of Race)
            Get
                Return m_Races
            End Get
            Set(value As List(Of Race))
                m_Races = value
            End Set
        End Property

        <XmlIgnore()> _
        Public Property Frm() As FrmMain
            Get
                Return m_Frm
            End Get
            Set(value As FrmMain)
                m_Frm = value
            End Set
        End Property
#End Region

#Region " Constructor "
        Sub New()
            m_Races = New List(Of Race)
        End Sub
#End Region

#Region " Methods "
        ''' <summary>
        ''' Enregistre l'état courant de la classe dans un fichier au format XML.
        ''' </summary>
        Public Sub SaveFile()
            Dim serializer As New XmlSerializer(GetType(AppSettings))
            Dim Save As New StreamWriter(Application.StartupPath & "\CompetitionBDD.xml")
            serializer.Serialize(Save, Me)
            Save.Close()
        End Sub

        ''' <summary>
        ''' Charge l'état courant du fichier XML.
        ''' </summary>
        ''' <returns>Valeur de type AppSettings</returns>
        Public Shared Function LoadFile() As AppSettings
            Dim deserializer As New XmlSerializer(GetType(AppSettings))
            Dim read As New StreamReader(Application.StartupPath & "\CompetitionBDD.xml")
            Dim p As AppSettings = DirectCast(deserializer.Deserialize(read), AppSettings)
            read.Close()
            Return p
        End Function

        Public Sub LoadRaces()
            m_Frm.LvRaces.Items.Clear()
            m_Frm.LvMembers.Items.Clear()
            m_Frm.LvArrived.Items.Clear()
            m_Frm.TsBtnRaceRemove.Enabled = False
            m_Frm.TsBtnRaceModify.Enabled = False
            m_Frm.LblTitle.Text = If(m_Races.Count = 0, "Veuillez créer une course", "Veuillez sélectionner une course")
            For Each Race In m_Races
                Dim lvi As New ListViewItem(New String(4) {Race.Name, Race.Date, Race.Distance, Race.Place, Race.Comment})
                lvi.Tag = Race
                m_Frm.LvRaces.Items.Add(lvi)
            Next
            m_Frm.GbxRaces.Text = "Courses (" & m_Races.Count.ToString & ")"
        End Sub

        Public Sub LoadRace(Name$)
            Dim race = m_Races.FirstOrDefault(Function(f) f.Name = Name)
            If Not race Is Nothing Then
                m_Frm.LvMembers.Items.Clear()
                m_Frm.GbxMembers.Enabled = True
                m_Frm.TsBtnRaceRemove.Enabled = True
                m_Frm.TsBtnRaceModify.Enabled = True
                With race
                    m_Frm.LblTitle.Text = .Name
                    For Each Member In .Members
                        Dim lvi As New ListViewItem(New String(7) {Member.Bib, Member.Name, Member.FirstName, Member.Sex, Member.Category, Member.BirthDate, Member.Club, Member.Comment})
                        lvi.Tag = Member
                        m_Frm.LvMembers.Items.Add(lvi)
                    Next
                    m_Frm.PnlMain.Enabled = race.Members.Count > 0
                    m_Frm.GbxMembers.Text = "Participants (" & .Members.Count.ToString & ")"
                End With
            Else
                m_Frm.GbxMembers.Enabled = False
                m_Frm.TsBtnRaceRemove.Enabled = False
                m_Frm.TsBtnRaceModify.Enabled = False
            End If
        End Sub

        Public Sub DeleteRace()
            m_Races.Remove(Frm.CurrentRace)
            SaveFile()
            m_Frm.GbxMembers.Enabled = False
            m_Frm.PnlMain.Enabled = False
            m_Frm.LblElapsed.Text = "00:00:00.00"
        End Sub

#End Region

    End Class
End Namespace

